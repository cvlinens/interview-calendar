# == Route Map
#
#                   Prefix Verb   URI Pattern                    Controller#Action
#               user_index GET    /user/index(.:format)          user#index
#              admin_index GET    /admin/index(.:format)         admin#index
#           calendar_index GET    /calendar/index(.:format)      calendar#index
#         new_user_session GET    /users/sign_in(.:format)       users/sessions#new
#             user_session POST   /users/sign_in(.:format)       users/sessions#create
#     destroy_user_session GET    /users/sign_out(.:format)      users/sessions#destroy
#        new_user_password GET    /users/password/new(.:format)  users/passwords#new
#       edit_user_password GET    /users/password/edit(.:format) users/passwords#edit
#            user_password PATCH  /users/password(.:format)      users/passwords#update
#                          PUT    /users/password(.:format)      users/passwords#update
#                          POST   /users/password(.:format)      users/passwords#create
# cancel_user_registration GET    /users/cancel(.:format)        users/registrations#cancel
#    new_user_registration GET    /users/sign_up(.:format)       users/registrations#new
#   edit_user_registration GET    /users/edit(.:format)          users/registrations#edit
#        user_registration PATCH  /users(.:format)               users/registrations#update
#                          PUT    /users(.:format)               users/registrations#update
#                          DELETE /users(.:format)               users/registrations#destroy
#                          POST   /users(.:format)               users/registrations#create
#                    users GET    /users(.:format)               users#index
#                          POST   /users(.:format)               users#create
#                 new_user GET    /users/new(.:format)           users#new
#                edit_user GET    /users/:id/edit(.:format)      users#edit
#                     user GET    /users/:id(.:format)           users#show
#                          PATCH  /users/:id(.:format)           users#update
#                          PUT    /users/:id(.:format)           users#update
#                          DELETE /users/:id(.:format)           users#destroy
#                     root GET    /                              users#index
#                 calendar GET    /calendar(.:format)            admin#index

Rails.application.routes.draw do
  get 'user/index'

  get 'admin/index'

  get 'calendar/index'

  devise_scope :user do
    get "/users/edit", to: "users/registrations#edit", as: "edit_user_registration"
    put "/users", to: "users/registrations#update", as: "user_registration"
    # get "/users/cancel", to: "users/registrations#cancel", as: "cancel_user_registration"
    get "/sign_in" => "users/sessions#new"
    get '/users/sign_out' => 'users/sessions#destroy'
    delete "/users", to: "users/registrations#destroy"

    get "check_username", to: "users/sessions#check_username"
    get "check_email", to: "users/sessions#check_email"
    get "check_email_registration", to: "users/sessions#check_email_registration"
    post "/users/new" => "users#new", as: "new_users"
    post "/change_role" => "users#change_role", as: "user_change_role"
    post "/suspend/:id" => "users#suspend", as: "user_suspend"
    post "/reactivate/:id" => "users#reactivate", as: "user_reactivate"
    post "/check_user_email" => "users#check_user_email", as: "check_user_email"
    post "/check_user_name" => "users#check_user_name", as: "check_user_name"
    get "/inactive_status" => "users#inactive_status", as: "inactive_status"
  end
  # devise_for :users, skip: :registration
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations', passwords: 'users/passwords' }, skip: :registration
  resources :users

  root :to => 'events#summary'

  get "/calendar" => "events#calendar"

  post "/create_event" => "events#create", as: "create_events"
  get "/get_event" => "events#get", as: "get_events"
  post "/update_event" => "events#update", as: "update_events"
  get "/events" => "events#index", as: "events"
  post "/events" => "events#index", as: "events_sort"
  post "/change" => "events#change", as: "change_events"
  get "/events/:event_id" => "events#edit", as: "edit_events"
  delete "/delete_event" => "events#destroy", as: "delete_events"
  # get "/myevents" => "events#my_events", as: "my_events"
  post "/comment" => "events#comment", as: "comment_events"
  get "/events/user_events/:user_id" => "events#user_events", as: "user_events"
  post "/events/user_events/:user_id" => "events#user_events", as: "user_events_sort"

  get "/report" => "events#report", as: "report"
  get "/report_filter" => "events#report_filter", as: "report_filter"
  post "/summary_filter" => "events#summary_filter", as: "summary_filter"
  resources :categories
  
  resources :shifts
  get "/shifts/view" => "shifts#show", as: "view_shift"
  
  resources :lunchtimes
  get "/lunchtimes/view" => "lunchtimes#show", as: "view_lunchtime"

  resources :points do
    get :history, on: :collection
  end
  
  
  resources :penalties

  get "/penalties_summary" => "penalties#summary", as: "penalties_summary"
  get "/penalties/user/:user_id" => "penalties#user", as: "penalties_user"
  get "/penalties/your-penalties" => "penalties#show", as: "show"

  resources :consequences

  post "/penalties/confirmed" => "penalties#confirm_penalty", as: "confirm_penalty"
  get "/help" => "helps#index", as: "help"
  resources :holidays
  resources :holidaytypes
  resources :roles
  resources :departments do
    get :show_users, on: :collection
  end
  post "/call_sub" => "departments#call_sub", as: "call_sub"
end
