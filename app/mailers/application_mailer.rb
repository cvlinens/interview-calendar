class ApplicationMailer < ActionMailer::Base
  default from: 'support@cvlinens.com'
  layout 'mailer'
end
