class UserMailer < ApplicationMailer
  default from: 'notifications@cvlinens.com'
 
  def create_event_notification(user, event, start_D, end_D)
    @user = user
    @event = event
    @start_D = start_D
    @end_D = end_D

    @default_time_start = Dates.defaultTimezone(@start_D)
    @default_time_end = Dates.defaultTimezone(@end_D)

    @admin = User.find_by(role_id: 3)
    if @event.user.admin?
      @manager = nil
    else
      @manager = event.user.find_own_manager
    end

    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@manager.try(:full_name)} <#{@manager.try(:email)}>", 
      cc: "#{@admin.try(:full_name)} <#{@admin.try(:email)}>", 
      subject: "#{@user.full_name} created new event"
    )
  end

  def change_status_event_notification(current_user, event, updated_status, start_D, end_D)
    user = event.user
    @event = event
    @start_D = start_D
    @end_D = end_D

    @default_time_start = Dates.defaultTimezone(@start_D)
    @default_time_end = Dates.defaultTimezone(@end_D)

    if @event.user.admin?
      manager = nil
    else
      manager = event.user.find_own_manager
    end
    @current = current_user
    admin = User.find_by(role_id: 3)
    @status = updated_status

    if @status == 3
      @changed = "approved"
    elsif @status == 2
      @changed = "denied"
    else
      @changed = "canceled"
    end

    if @current.admin?
      if @event.user.admin?
        return false
      elsif @event.user.manager?
        @to_user = manager
        @to_cc = nil
      else
        @to_user = user
        @to_cc = manager
      end
    elsif @current.manager?
      if @status == 1 # manager cancel own event
        @to_user = admin
        @to_cc = nil
      elsif @status == 2  #manager deny employee event
        @to_user = user
        @to_cc = admin
      else              # manager approve employee event
        @to_user = user
        @to_cc = admin
      end
    else
      @to_user = manager
      @to_cc = admin
    end

    mail(from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.try(:full_name)} <#{@to_user.try(:email)}>", 
      cc: "#{@to_cc.try(:full_name)} <#{@to_cc.try(:email)}>", 
      subject: "#{@current.full_name} #{@changed} event"
    )
  end

  def update_event_notification(event, user, start_D, end_D)
    @event = event
    @user = user
    @start_D = start_D
    @end_D = end_D

    @default_time_start = Dates.defaultTimezone(@start_D)
    @default_time_end = Dates.defaultTimezone(@end_D)

    admin = User.find_by(role_id: 3)
    if @event.user.admin?
      manager = nil
    else
      manager = event.user.find_own_manager
    end
    
    if @user.admin?
      if @event.user.admin?
        return false
      elsif @event.user.manager?
        @to_user = manager
        @to_cc = nil
      else
        @to_user = @event.user
        @to_cc = manager
      end
    elsif @user.manager?
      if @event.user.manager?
        @to_user = admin
        @to_cc = nil
      else
        @to_user = @event.user
        @to_cc = admin
      end
    else
      @to_user = manager
      @to_cc = admin
    end
    mail(from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.try(:full_name)} <#{@to_user.try(:email)}>", 
      cc: "#{@to_cc.try(:full_name)} <#{@to_cc.try(:email)}>", 
      subject: "#{@user.full_name} updated event"
    )
  end

  def create_comment_notification(user, event, left_time, start_D, end_D)
    @event = event
    @user = user
    @left_time = left_time
    @start_D = start_D
    @end_D = end_D

    @default_time_start = Dates.defaultTimezone(@start_D)
    @default_time_end = Dates.defaultTimezone(@end_D)

    if @user.admin?
      @to_user = @event.user
      @to_cc = @event.user.find_own_manager
    else
      @to_user = @event.user
      @to_cc = User.find_by(role_id: 3)
    end
    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.try(:full_name)} <#{@to_user.try(:email)}>", 
      cc: "#{@to_cc.try(:full_name)} <#{@to_cc.try(:email)}>", 
      subject: "#{@user.full_name} left comment"
    )
  end

  def delete_event_notification(event, current_user)
    @event = JSON.parse(event, object_class: OpenStruct)
    deleted_time = Time.now.utc
    @deleted_time = Dates.d_Date(deleted_time)
    @to_user = User.find @event.user_id
    @current = current_user
    @default_time_start = Dates.defaultTimezone(Dates.d_Date(DateTime.parse(@event.start_date)))
    @default_time_end = Dates.defaultTimezone(Dates.d_Date(DateTime.parse(@event.end_date)))
    @category = Category.find @event.category_id
    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.full_name} <#{@to_user.email}>", 
      subject: "#{@current.full_name} deleted your event"
    )
  end

  def create_signup_notification(full_name, email, username)
    @full_name = full_name
    @email_address = email
    @username = username
    admin = User.find_by(role_id: 3)

    @created_time = Dates.defaultTimezone(Dates.d_Date(Time.now.utc))

    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{admin.try(:full_name)} <#{admin.try(:email)}>", 
      subject: "#{@full_name} signup new account"
    )
  end

  def create_user_notification(current, new_user, current_password, created_time)
    @current = current
    @to_user = new_user
    @current_password = current_password

    @created_time = Dates.defaultTimezone(Dates.d_Date(Time.now.utc))

    if (@to_user.admin? || @to_user.manager?)
      @to_cc = nil
    else
      @to_cc = @to_user.find_own_manager
    end
    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.try(:full_name)} <#{@to_user.try(:email)}>", 
      cc: "#{@to_cc.try(:full_name)} <#{@to_cc.try(:email)}>", 
      subject: "#{@current.full_name} created new user"
    )
  end

  ## done
  def change_user_info_notification(current, user) 
    @current = current
    @to_user = user
    @to_cc = @to_user.find_own_manager
    @updated_time = Dates.defaultTimezone(Dates.d_Date(@to_user.updated_at))

    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.try(:full_name)} <#{@to_user.try(:email)}>", 
      cc: "#{@to_cc.try(:full_name)} <#{@to_cc.try(:email)}>", 
      subject: "#{@current.full_name} updated user info"
    )
  end

  def delete_user_notification(deleted, current)
    @current = current
    @to_user = JSON.parse(deleted, object_class: OpenStruct)
    @to_user_full_name = "#{@to_user.first_name} #{@to_user.last_name}"
    @deleted_time = Dates.defaultTimezone(Dates.d_Date(Time.now.utc))
    mail(from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user_full_name} <#{@to_user.email}>", 
      subject: "#{@current.full_name} deleted user"
    )
  end

  def contactsupport_email
    
  end

  def suspend_user_notification(current, user, reason)
    @current = current
    @to_user = user
    @reason = reason
    @suspend_time = Dates.defaultTimezone(Dates.d_Date(Time.now.utc))
    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.full_name} <#{@to_user.email}>", 
      subject: "Your account has been suspended"
    )
  end

  def reactivate_user_notification(current, user)
    @current = current
    @to_user = user

    @reactivate_time = Dates.defaultTimezone(Dates.d_Date(Time.now.utc))
    mail(
      from: "CVLinens Calendar <no-reply@cvlinens.com>", 
      to: "#{@to_user.full_name} <#{@to_user.email}>", 
      subject: "Your account has been reactivated"
    )
  end

  private

    def custom_email
      
    end
end
