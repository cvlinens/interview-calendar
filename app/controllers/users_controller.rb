class UsersController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  before_action :check_active_status, except: [:inactive_status]
  before_action :check_permission, except: [:inactive_status, :reactivate, :destroy, :suspend]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.user_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @users = User.all.order(:id)
    @user = current_user
  end

  def new
    redirect_to users_path
    return true

    # new_user = User.new user_params
    # current_password = user_params[:password]
    # if new_user.save
    #   flash[:success] = I18n.t 'new_user_create_success'
    #   redirect_to users_path
    #   UserMailer.create_user_notification(current_user, new_user, current_password).deliver_later
    # else
    #   flash[:alert] = I18n.t 'new_user_create_fail'
    #   redirect_to users_path
    # end
    
  end

  def edit
    @user = user
    
  end
  
  def update
    if user.update_attributes(user_params)
      flash[:success] = I18n.t 'user_update_success'
      redirect_to users_path
      if !user.admin?
        UserMailer.change_user_info_notification(current_user, user).deliver_later
      end
    else
      flash[:alert] = I18n.t 'user_update_fail'
    end
    
    # render :edit
  end

  def check_user_name
    username = params[:username]
    user = User.find_by(username: username)
    if user.present?
      respond = {
        status: "existing"
      }
    else
      respond = {
        status: "new"
      }
    end
    render json: respond
  end

  def check_user_email
    email = params[:email]
    user = User.find_by(email: email)
    if user.present?
      respond = {
        status: "existing"
      }
    else
      respond = {
        status: "new"
      }
    end
    render json: respond
  end

  def change_role
    roleID = params[:getRoleID].to_i
    if roleID == 1   # employee => find 'manager'
      @managers = User.where(role_id: 2)
      @departments = Department.all_sort
    elsif roleID == 2   # manager => find 'admin'
      @managers = User.where(role_id: 3)
      @departments = Department.all_sort
    else 
      #find user

    end
    managers = []
    @managers.each do |manager|
      item = {}
      %i(id).each do |i|
        item[i] = manager.id
        item[:full_name] = manager.full_name
      end
      managers << item
    end
    departments = []
    @departments.each do |department|
      item = {}
      %i(id).each do |i|
        item[:id] = department.id
        item[:name] = department.name
        item[:check_level] = department.check_level
        item[:has_child] = department.has_child?
        item[:is_root] = department.is_root?
        item[:parent_id] = department.parent_id
      end
      departments << item
    end
    respond = {
      managers: managers,
      departments: departments,
      roleID: roleID
    }
    render json: respond
  
  end

  def suspend
    redirect_to users_path if current_user == user
    if current_user.admin?
      reason = params[:reason]
      if user.update_attribute('active', false)
        UserMailer.suspend_user_notification(current_user, user, reason).deliver_later
        flash[:notice] = I18n.t 'user_suspend_success'
      else
        flash[:alert] = I18n.t 'user_suspend_fail'
      end
      redirect_to users_path
    else
      flash[:alert] = I18n.t 'no_permission_user_suspension'
      redirect_to root_path
    end

  end

  def reactivate
    redirect_to users_path if current_user == user

    if current_user.admin?
      if user.update_attribute('active', true)
        UserMailer.reactivate_user_notification(current_user, user).deliver_later
        flash[:notice] = I18n.t 'user_reactivate_success'
      else
        flash[:alert] = I18n.t 'user_reactivate_fail'
      end
      redirect_to users_path
    else
      flash[:alert] = I18n.t 'no_permission_user_reactivate'
      redirect_to root_path
    end
  end

  def inactive_status
    
  end

  def destroy
    redirect_to root_path
    flash[:alert] = I18n.t 'suspened_user_delete'
    # if current_user.admin?
    #   user.update_attribute(:current_user, current_user) # current_user for model
    #   @destroyed_user = user.to_json
    #   if user.destroy
    #     flash[:notice] = I18n.t 'user_destroy_success'
    #     redirect_to users_path
    #     UserMailer.delete_user_notification(@destroyed_user, current_user).deliver_later
    #   else
    #     flash[:alert] = I18n.t 'user_destroy_fail'
    #   end
    # else
    #   redirect_to root_path
    #   flash[:alert] = I18n.t 'no_permission_user_delete'
    # end
    
  end

  protected
    def user
      @user ||= User.friendly.find(params[:id]) 
    end

  private

    def user_params
      params.require(:user).permit(:id, :first_name, :last_name, :username, :email, :role_id, :manager_id, :time_zone, :shift_id, :lunchtime_id, :password, :password_confirmation, department_ids: [])
    end
end
