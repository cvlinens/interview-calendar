module Dates
  extend ActiveSupport::Concern

  def self.d_date(d)
    d.strftime("%B %e, %Y")
  end

  def self.t_time(d)
    d.strftime("%l:%M %p")
  end

  def self.d_Date(d)
    "#{d_date(d)} #{t_time(d)}"
  end

  def self.defaultTimezone(d)
    tz = "Central Time (US & Canada)"
    dd_date = DateTime.parse(d).utc
    dd = dd_date.in_time_zone(tz)
    return "#{d_Date(dd)}"
  end

  def self.defaultDeliveryTime(d)
    tz = "Central Time (US & Canada)"
    dd_date = DateTime.parse(d).utc
    dd = dd_date.in_time_zone(tz)
    return "#{dd.strftime("%b %d,%l:%M %p")}"
  end

  def self.convertedTime(d, tz)
    d ||= Date.today
    c_time = d.in_time_zone(tz)
    "#{d_date(c_time)} #{t_time(c_time)}"
  end


  def self.time_12(start_t, end_t)
    int_start = start_t.split(':')
    if int_start[0].to_i == 12
      str_start = start_t + ' PM'
    elsif int_start[0].to_i > 12
      int_start_small = int_start[0].to_i - 12
      str_start = int_start_small.to_s + ':' + int_start[1] + ' PM'
    else
      str_start = start_t + ' AM'
    end

    int_end = end_t.split(':')
    if int_end[0].to_i == 12
      str_end = end_t + ' PM'
    elsif int_end[0].to_i > 12
      int_end_small = int_end[0].to_i - 12
      str_end = int_end_small.to_s + ':' + int_end[1] + ' PM'
    else
      str_end = end_t + ' AM'
    end
    return "#{str_start} - #{str_end}"
  end

end