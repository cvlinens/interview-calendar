class CategoriesController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  
  before_action :check_active_status
  before_action :check_permission, except: [:render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.category_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @categories_default = Category.all.where(default_option: true).order(name: :asc)
    @categories_non_default = Category.all.where(default_option: false).order(name: :asc)

    @categories = @categories_default + @categories_non_default
  end

  def new
    name = params[:category_name]
    description = params[:category_description]
    default_option = params[:default_option]

    if default_option.present?
      default_option = true
    else
      default_option = false
    end

    category = Category.new(name: name, description: description, default_option: default_option)

    if category.save
      flash[:notice] = I18n.t 'new_category_success'
      redirect_to categories_path
    else
      redirect_to categories_path
      flash[:alert] = I18n.t 'new_category_fail'
    end
  end

  def edit
    @categories_default = Category.all.where(default_option: true).order(name: :asc)
    @categories_non_default = Category.all.where(default_option: false).order(name: :asc)

    @categories = @categories_default + @categories_non_default

    @category = category
    @events = current_user.events_by_role.where(category_id: category.id)
  end

  def update
    name = params[:category_name]
    description = params[:category_description]
    default_option = params[:default_option]

    if default_option.present?
      default_option = true
    else
      default_option = false
    end

    if current_user.admin?
      if (category.update_attributes(name: name, description: description, default_option: default_option))
        redirect_to edit_category_path
        flash[:notice] = I18n.t 'edit_category_success'
      else
        redirect_to edit_category_path
        flash[:alert] = I18n.t 'edit_category_fail'
      end
    end
  end

  def destroy
    category_id = category.id
    if current_user.admin?
      if category.default_option? and Category.where(default_option: true).count == 1
        flash[:alert] = I18n.t 'no_permission_last_default_category'
      else
        if category.event.blank?
          if category.destroy
            events = Event.where(category_id: category_id)
            events.each do |event|
              if event.update_attribute('category_id', 0)
                puts "OKay"
              else
                puts "no"
              end
            end
            flash[:notice] = I18n.t 'delete_category_success'
          else
            flash[:alert] = I18n.t 'delete_category_fail'
          end
        else
          flash[:alert] = I18n.t 'delete_category_no_empty'
        end
      end
    else
      flash[:alert] = I18n.t 'no_permission_category_creation'
    end

    redirect_to categories_path
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  protected
    def category
      @category ||= Category.friendly.find(params[:id]) 
    end

end
