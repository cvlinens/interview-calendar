class PenaltiesController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  before_action :check_active_status
  before_action :check_permission, except: [:show, :render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.penalty_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @points = Point.all
    @enabled_points = Point.all.where(disabled: false).order(name: :asc)
    @penalties = Penalty.all.order(apply_date: :desc)
    @penalty = Penalty.new
  end

  def new
    @penalty = Penalty.new
  end

  def create
    user_id = params[:penalty][:user_id]
    point_id = params[:penalty][:point_id]
    description = params[:penalty][:description]
    apply_date = params[:penalty][:apply_date]
    h = Time.now.hour
    m = Time.now.min
    s = Time.now.sec
    apply_date = DateTime.parse(apply_date).change(hour: h, min: m, sec: s)
    apply_date_tz = apply_date.asctime.in_time_zone(current_user.time_zone)
    apply_date_UTC = apply_date_tz.utc

    user = User.find(user_id)
    penalty = Penalty.new user_id: user_id, point_id: point_id, description: description, apply_date: apply_date_UTC

    if penalty.save
      # user.apply_penalty(point_id)
      if user.check_penalty?
        redirect_to penalties_summary_path
        flash[:alert] = I18n.t 'reach_penalty_success'
      else
        flash[:notice] = I18n.t 'new_penalty_success'
        redirect_to penalties_path
      end
    else
      flash[:alert] = I18n.t 'new_penalty_fail'
      redirect_to penalties_path
    end
  end

  def edit
    @points = Point.all
    @enabled_points = Point.all.where(disabled: false).order(name: :asc)
    @penalties = Penalty.all.order(apply_date: :desc)
    @penalty = penalty
  end

  def update
    user_id = params[:penalty][:user_id]
    point_id = params[:penalty][:point_id]
    description = params[:penalty][:description]
    apply_date = params[:penalty][:apply_date]
    h = Time.now.hour
    m = Time.now.min
    s = Time.now.sec
    apply_date = DateTime.parse(apply_date).change(hour: h, min: m, sec: s)
    apply_date_tz = apply_date.asctime.in_time_zone(current_user.time_zone)
    apply_date_UTC = apply_date_tz.utc

    if penalty.update_attributes user_id: user_id, point_id: point_id, description: description, apply_date: apply_date_UTC
      flash[:notice] = I18n.t 'edit_penalty_success'
    else
      flash[:alert] = I18n.t 'edit_penalty_fail'
    end
    redirect_to edit_penalty_path
  end

  def summary
    @users = User.all.order(:first_name)
    @penalties = Penalty.all

    filtered_datas = []
    consequences = Consequence.all
    consequences.each do |consequence|
      consequence_point = consequence.reach_point
      if consequence == Consequence.last
        break
      else
        consequence_next_point = consequence.next.reach_point
      end
      filtered = []
      User.all.each do |u|
        if u.total_penalty >= consequence_point && u.total_penalty < consequence_next_point
          if u.consequence_id != consequence.id
            filtered << u
            filtered << consequence
            filtered_datas << filtered
          end
        end
      end
    end
    @filtered_datas = filtered_datas
    
  end

  def confirm_penalty
    id = params[:id]
    consequence_id = params[:consequence_id]
    user = User.find(id)
    user.confirm_penalty(consequence_id)
    data = {
      user_id: user.id,
      flag_color: user.consequence.flag_color,
      consequence_full_info: user.consequence.full_info
    }
    render json: data
    
  end

  def user
    user_id = params[:user_id]
    @user = User.find_by(slug: user_id)
    @penalties = @user.penalties
    
  end

  def show
    user_id = params[:user_id]
    @user = User.find_by(slug: user_id)
  end

  def destroy
    user_id = penalty.user_id
    user = User.find(user_id)

    if penalty.destroy
      # if user.consequence_id != 0
      #   user.update_consequence_id
      # end
      flash[:notice] = I18n.t 'delete_penalty_success'
    else
      flash[:alert] = I18n.t 'delete_penalty_fail'
      render_404
    end
    redirect_to penalties_path
    
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  protected
    def penalty
      @penalty ||= Penalty.find(params[:id])
    end

  # private
  #   def penalty_params
  #     params.require(:penalty).permit(:id, :user_id, :point_id, :description, :apply_date)
  #   end
end
