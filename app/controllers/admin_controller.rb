class AdminController < ApplicationController
  before_action :authenticate_user!
  layout "admin"
  

  def index
    @user = current_user
  end

  def dashboard

  end

  private

    def user_params
      params.require(:user).permit(:id, :email, :first_name, :last_name, :username, :role_id, :manager_id, :token)
    end
end
