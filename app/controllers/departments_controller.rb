class DepartmentsController < ApplicationController
  before_action :authenticate_user!, except: []
  before_action :check_active_status
  before_action :check_permission, except: [:show_users]

  layout "index"

  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.department_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @departments = Department.managers
    @department = Department.new
  end

  def show_users
    redirect_to departments_path unless current_user.manager? && !current_user.department_perm?

    @departments = current_user.departments

  end
  
  def call_sub
    sub_departments = Department.all.where(parent_id: department.id).order(name: :asc)
    # respond = {
    #   id: sub
    # }
    render json: sub_departments.to_json
  end

  def new
    @department = Department.new
  end

  def create
    department = Department.new department_params

    if department.save
      flash[:notice] = I18n.t 'new_department_success'
    else
      flash[:notice] = I18n.t 'new_department_fail'
    end
    redirect_to departments_path
  end

  def edit
    @department = department
    # @departments = Department.managers
    @users = department.related_users.order(first_name: :asc)
  end

  def update
    if (department.update_attributes department_params)
      flash[:notice] = I18n.t 'edit_department_success'
    else
      flash[:alert] = I18n.t 'edit_department_fail'
    end
    
    redirect_to edit_department_path
  end

  def destroy
    is_root = false
    if department.is_root?
      parent_id = department[:id]
      is_root = true
    end
    if !department.has_child?
      if department.users.blank?
        if department.destroy
          if is_root
            ds = Department.where(parent_id: parent_id)
            ds.each do |d|
              d.update_column :parent_id, nil
            end
          end
          flash[:notice] = I18n.t 'delete_department_success'
        else
          flash[:alert] = I18n.t 'delete_department_fail'
        end
      else
        flash[:alert] = I18n.t 'delete_department_no_empty_user'
      end
    else
      flash[:alert] = I18n.t 'delete_department_no_empty_child'
    end
    redirect_to departments_path
  end

  protected
    def department
      @department ||= Department.find(params[:id])
    end

  private
    def department_params
      params.require(:department).permit(:id, :name, :parent_id)
    end
end
