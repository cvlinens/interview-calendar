# module Current
#   thread_mattr_accessor :user
# end

class ApplicationController < ActionController::Base


  protect_from_forgery with: :exception
  skip_before_action  :verify_authenticity_token
  before_action :configure_permitted_parameters, if: :devise_controller?

  helper_method :redirect_url

  def redirect_url
    return new_patron_session_path unless patron_signed_in?
    case current_patron
      when User
        root_path
      when Admin
        root_path
    end
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  rescue
    render_404
  end

  def render_404
    render file: "#{Rails.root}/public/404", status: :not_found
  end

  def after_sign_in_path_for(resource)
    root_path
  end

  protected

    def configure_permitted_parameters
      # devise 4.3 .for method replaced by .permit
      devise_parameter_sanitizer.permit(:sign_in, keys: [:username])
      # devise_parameter_sanitizer.for(:sign_in) << :username
    end
    # def configure_permitted_parameters
    # 	devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:username, :email, :first_name, :last_name, :password, :password_confirmation, :role_id, :manager_id) }
    #   devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me, :role_id, :manager_id) }
    #   devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:id, :username, :email, :first_name, :last_name,:role_id, :manager_id, :time_zone, :shift_id, :lunchtime_id, :password, :password_confirmation, :current_password) }
    # end
end
