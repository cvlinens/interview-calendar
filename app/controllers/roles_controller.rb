class RolesController < ApplicationController
  before_action :authenticate_user!, except: []
  # before_action :generate_unique_token, only: [:create]
  layout 'index'
  
  before_action :check_active_status
  before_action :check_permission, except: [:show, :render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.user_role_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @roles = Role.where('id <= 3').order(id: :desc) + Role.where('id > 3').order(name: :asc)
    @role = Role.new
  end

  def new
    @role = Role.new
  end

  def create
    role = Role.new role_params
    if role.save
      flash[:notice] = I18n.t 'new_role_success'
    else
      flash[:alert] = I18n.t 'new_role_fail'
    end
    redirect_to roles_path
  end

  def edit
    redirect_to roles_path if role.id == 3

    @role = role
    @roles = Role.where('id <= 3').order(id: :desc) + Role.where('id > 3').order(name: :asc)
  end

  def update
    redirect_to roles_path if role.id == 3
    
    if (role.update_attributes role_params)
      flash[:notice] = I18n.t 'edit_role_success'
    else
      flash[:alert] = I18n.t 'edit_role_fail'
    end
    redirect_to edit_role_path
  end

  def destroy
    redirect_to roles_path if role.id <= 3

    role_id = role.id
    if role.destroy
      
      flash[:notice] = I18n.t 'delete_role_success'
    else
      flash[:alert] = I18n.t 'delete_role_fail'
    end
    redirect_to roles_path
  end

  protected
    def role
      @role ||= Role.find(params[:id])
    end
  
  private
    def role_params
      params.require(:role).permit(:id, :name, :full_access, :user, :user_role, :department, :event_creation, :event_status, :category, :shift, :lunchtime, :holiday, :penalty, :point, :consequence)
    end
end
