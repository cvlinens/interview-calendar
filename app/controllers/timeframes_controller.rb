class TimeframesController < ApplicationController
  before_action :authenticate_user!, except: []
  before_action :check_active_status
  
  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end
  
end
