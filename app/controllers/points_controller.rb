class PointsController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  before_action :check_active_status
  before_action :check_permission, except: [:show, :render_404]
  include Dates

  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.point_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @points = Point.all.where(disabled: false).order(name: :asc)
    
  end

  def new
    name = params[:point_name]
    description = params[:description]
    penalty_point = params[:penalty_point]
    original_id = params[:original_id]
    find_point = Point.find_by(name: name)

    if original_id.present?
      original_point = Point.find(original_id)

      if find_point.present?
        if find_point.id == original_point.id
          original_name = name + ' - <disabled - ' + Time.now.to_i.to_s + '>'
          original_point.update_column :name, original_name
          point = Point.new(name: name, description: description, penalty_point: penalty_point, original_id: original_id)
          if point.save
            flash[:notice] = I18n.t 'edit_point_success'
            original_point.update_column :disabled, true
            point.update_column :updated_user, current_user.id
          else
            original_name = name
            original_point.update_column :name, original_name
            flash[:alert] = I18n.t 'new_point_fail'
          end
        else
          flash[:alert] = I18n.t 'existing_point_name'
        end
      else
        point = Point.new(name: name, description: description, penalty_point: penalty_point, original_id: original_id)
        if point.save
          flash[:notice] = I18n.t 'edit_point_success'
          original_point.update_column :disabled, true
          point.update_column :updated_user, current_user.id
        else
          flash[:alert] = I18n.t 'new_point_fail'
        end
      end
    else
      if find_point.blank?
        point = Point.new(name: name, description: description, penalty_point: penalty_point, original_id: original_id)
        if point.save
          point.update_column :updated_user, current_user.id
          flash[:notice] = I18n.t 'new_point_success'
        else
          flash[:alert] = I18n.t 'new_point_fail'
        end
      else
        flash[:alert] = I18n.t 'existing_point_name'
      end
    end
    
    redirect_to points_path
  end

  def edit
    @point = point
    @points = Point.all.where(disabled: false).order(name: :asc)
  end

  def update
    name = params[:point_name]
    description = params[:description]
    penalty_point = params[:penalty_point]

    if current_user.admin?
      if (point.update_attributes(name: name, description: description, penalty_point: penalty_point))
        point.penalties.each do |p|
        end
        flash[:notice] = I18n.t 'edit_point_success'
      else
        flash[:alert] = I18n.t 'edit_point_fail'
      end
    end
    redirect_to edit_point_path
  end

  def show
  end

  def history
    point_ids = Point.all.where(disabled: false).order(name: :asc).reject {|n| n.history.count == 0}.pluck(:id)
    @points = Point.where(id: point_ids).order(created_at: :desc)
  end

  def destroy
    if point.destroy
      flash[:notice] = I18n.t 'delete_point_success'
    else
      flash[:alert] = I18n.t 'delete_point_fail'
    end
    redirect_to points_path
  end

  protected
    def point
      @point ||= Point.friendly.find(params[:id])
    end

end
