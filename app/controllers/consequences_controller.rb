class ConsequencesController < ApplicationController
  before_action :authenticate_user!, except: []
  before_action :check_active_status
  layout "index"

  before_action :check_permission
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.consequence_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @consequences = Consequence.all
  end
  
  def new
    reach_point = params[:reach_point]
    description = params[:description]
    flag_color = params[:flag_color]

    consequence = Consequence.new(reach_point: reach_point, description: description, flag_color: flag_color)
    if consequence.save
      flash[:notice] = I18n.t 'new_consequence_success'
    else
      flash[:alert] = I18n.t 'new_consequence_fail'
    end
    redirect_to consequences_path
  end

  def edit
    @consequence = consequence
    @consequences = Consequence.all.order(reach_point: :asc)
  end

  def update
    if consequence.update_attributes consequence_params
      flash[:notice] = I18n.t 'edit_consequence_success'
    else
      flash[:alert] = I18n.t 'edit_consequence_fail'
    end
    redirect_to edit_consequence_path
  end

  def destroy
    if consequence.users.blank?
      if consequence.destroy
        flash[:notice] = I18n.t 'delete_consequence_success'
      else
        flash[:alert] = I18n.t 'delete_consequence_fail'
      end
    else
      flash[:alert] = I18n.t 'delete_consequence_no_empty'
    end

    redirect_to consequences_path
  end

  protected
    def consequence
      @consequence ||= Consequence.find(params[:id])
    end

  private
    def consequence_params
      params.require(:consequence).permit(:id, :reach_point, :description, :flag_color)
    end
end
