class HelpsController < ApplicationController
  layout "blank"

  def index
    @categoris = Category.all
    @shifts = Shift.all
    @lunchtimes = Lunchtime.all
    @consequences = Consequence.all
    @points = Point.all
  end
end
