class ShiftsController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  include Dates
  before_action :check_active_status, except: [:show, :render_404]
  before_action :check_permission, except: [:show, :render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.shift_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @shifts = Shift.all.order(name: :asc)
    @shift = Shift.new
  end

  def new
    @shift = Shift.new
  end

  def create
    shift = Shift.new shift_params
    
    if shift.save
      flash[:notice] = I18n.t 'new_shift_success'
    else
      flash[:alert] = I18n.t 'new_shift_fail'
    end
    redirect_to shifts_path
  end

  def edit
    @shift = shift
    @shifts = Shift.all.order(name: :asc)
    @user = current_user
    @users = User.where(shift_id: shift.id).order(first_name: :asc)

  end

  def update

    if current_user.admin?
      if (shift.update_attributes shift_params)
        redirect_to edit_shift_path
        flash[:notice] = I18n.t 'edit_shift_success'
      else
        redirect_to edit_shift_path
        flash[:alert] = I18n.t 'edit_shift_fail'
      end
    end
  end

  def destroy
    shift_id = shift.id
    if shift.users.blank?
      if shift.destroy
        users = User.where(shift_id: shift_id)
        users.each do |user|
          if user.update_attribute('shift_id', 0)
          else
          end
        end
        flash[:notice] = I18n.t 'delete_shift_success'
      else
        flash[:alert] = I18n.t 'delete_shift_fail'
      end
    else
      flash[:alert] = I18n.t 'delete_shift_no_empty'
    end
    redirect_to shifts_path
  end

  def show
    @shifts = Shift.all
    shift_id = params[:query].presence || Shift.first.id
    @shift = Shift.find(shift_id)
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  protected
    def shift
      @shift ||= Shift.friendly.find(params[:id]) 
    end

  private
    def shift_params
      params.require(:shift).permit(:name, 
        timeframes_attributes: [
          :id, :week_name, :working_from, :working_to
        ]
      )
    end
end
