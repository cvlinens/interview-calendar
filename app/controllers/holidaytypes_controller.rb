class HolidaytypesController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  before_action :check_active_status
  before_action :check_permission, except: [:show, :render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.holiday_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
      @holidaytype = Holidaytype.new
      @holidaytypes = Holidaytype.all
    
  end

  def new
    
  end

  def create
      holidaytype = Holidaytype.new holidaytype_params

      if holidaytype.save
        flash[:notice] = I18n.t 'new_holidaytype_success'
      else
        flash[:alert] = I18n.t 'new_holidaytype_fail'
      end
      redirect_to holidaytypes_path
    
  end

  def edit
      @holidaytypes = Holidaytype.all.order(name: :asc)
      @holidaytype = holidaytype
    
  end

  def update
      if (holidaytype.update_attributes holidaytype_params)
        redirect_to edit_holidaytype_path
        flash[:notice] = I18n.t 'edit_holidaytype_success'
      else
        redirect_to edit_holidaytype_path
        flash[:alert] = I18n.t 'edit_holidaytype_fail'
      end
    
  end

  def destroy
      if holidaytype.destroy
        flash[:notice] = I18n.t 'delete_holidaytype_success'
        redirect_to holidaytypes_path
      else
        flash[:alert] = I18n.t 'delete_holidaytype_fail'
        redirect_to holidaytypes_path
      end
    
  end

  protected
    def holidaytype
      @holidaytype ||= Holidaytype.friendly.find(params[:id])
    end

  private
    def holidaytype_params
      params.require(:holidaytype).permit(:name, :hours_off, :duration, :start_time, :end_time)
    end
end
