class EventsController < ApplicationController
  before_action :authenticate_user!, except: []
  # before_action :generate_unique_token, only: [:create]
  layout 'index', only: [:index, :calendar, :edit, :user_events, :summary, :report, :event_filter]

  include Dates
  before_action :check_active_status
  
  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @filter_status = params[:filter].presence || 'pending'
    events = current_user.events_by_role
    if !current_user.admin?
      t_today = Time.now.in_time_zone(current_user.time_zone).beginning_of_day.utc
      events = current_user.events_by_role.where(status: 0).or(current_user.events_by_role.where('end_date >= ?', t_today))
    end
    if @filter_status == 'all'
      @events = events.order(created_at: :asc)
    else
      case @filter_status
      when 'approved'
        status = 3
      when 'denied'
        status = 2
      when 'canceled'
        status = 1
      when 'pending'
        status = 0
      else
        render_404

      end
      @events = events.where(status: status).order(created_at: :asc)
    end
    @time_zone = current_user.time_zone
    gon.time_zone = @time_zone
    gon.current_user_id = current_user.id
    gon.category_first_id = Category.where(default_option: true).first.id
    @category_default_first_id = Category.where(default_option: true).first.id
    gon.current_user_role = current_user.user_role
  end

  def summary
    @time_zone = current_user.time_zone

    @events = current_user.events_by_role
    @recent_events = @events.order(created_at: :desc).take(5)
    @total_events = @events.count
    @approved_events = @events.where(status: 3)
    @approved_events_count = @approved_events.count
    @denied_events = @events.where(status: 2)
    @denied_events_count = @denied_events.count
    @canceled_events = @events.where(status: 1)
    @canceled_events_count = @canceled_events.count
    @pending_events = @events.where(status: 0)
    @pending_events_count = @pending_events.count
    
    time_now = Time.now.in_time_zone(current_user.time_zone).strftime('%d %b %Y')
    d_time_now = DateTime.parse(time_now)
    utc_today_end = d_time_now.asctime.in_time_zone(current_user.time_zone).utc
    utc_today_start = utc_today_end + 1.days
    utc_today_start = utc_today_start.utc

    today_events = @approved_events.where('start_date <= ?', utc_today_start)
    @today_events = today_events.where('end_date >= ?', utc_today_end)

    if @events.present?
      @approved = (@approved_events_count.to_f / @total_events.to_f * 100).round(2)
      @denied = (@denied_events_count.to_f / @total_events.to_f * 100).round(2)
      @canceled = (@canceled_events_count.to_f / @total_events.to_f * 100).round(2)
      @pending = (@pending_events_count.to_f / @total_events.to_f * 100).round(2)

    else
      @approved = 0
      @denied = 0
      @canceled = 0
      @pending = 0
    end

    if current_user.admin? 
      @total_users_count = User.count
      @admin_count = User.where(role_id: 3).count
      @managers = User.where(role_id: 2)
      @manager_count = @managers.count
      @employee_count = User.where(role_id: 1).count
      
      managers_ids = User.where(role_id: 2).pluck(:id)
      @managers_events = @events.where(user_id: managers_ids).count

      employees_ids = User.where(role_id: 1).pluck(:id)
      @employees_events = @events.where(user_id: employees_ids).count

    elsif current_user.manager?
      temp_employees = User.where(manager_id: current_user.id).pluck(:id)
      employee_ids = User.where(manager_id: current_user.id).pluck(:id)
      @employee_count = employee_ids.count
      user_ids = temp_employees.push(current_user.id) # add self id(manager id)
      total_users = User.where(id: user_ids)
      @total_users_count = total_users.count
      @self_employees = User.where(id: employee_ids).order(first_name: :asc)
    else

    end

  end

  def summary_filter
    filter = params[:filter]

    @events = current_user.events_by_role
    @total_events = @events.count
        
    case filter
    when '0'
      start_date =  Time.new("2000-01-01").utc
      end_date = Time.new("2100-12-31").utc
    when '2'
      last_month = Time.now - 1.months
      start_date = last_month.in_time_zone(current_user.time_zone).beginning_of_month.utc
      end_date = last_month.in_time_zone(current_user.time_zone).end_of_month.utc
    when '1'
      start_date = Time.now.in_time_zone(current_user.time_zone).beginning_of_month.utc
      end_date = Time.now.in_time_zone(current_user.time_zone).end_of_month.utc
    else
      render_404
    end

    @approved_events = @events.where(status: 3)
    @approved_events_count = @approved_events.count
    @denied_events = @events.where(status: 2)
    @denied_events_count = @denied_events.count
    @canceled_events = @events.where(status: 1)
    @canceled_events_count = @canceled_events.count
    @pending_events = @events.where(status: 0)
    @pending_events_count = @pending_events.count
    
    total_events1 = @events.where('start_date < ?', start_date)
    total_1 = total_events1.where('end_date >= ?', start_date).count
    total_events2 = @events.where('start_date >= ?', start_date)
    total_2 = total_events2.where('start_date <= ?', end_date).count
    @total_events = total_1 + total_2

    filter_approved_events1 = @approved_events.where('start_date < ?', start_date)
    approved_1 = filter_approved_events1.where('end_date >= ?', start_date).count
    filter_approved_events2 = @approved_events.where('start_date >= ?', start_date)
    approved_2 = filter_approved_events2.where('start_date <= ?', end_date).count
    @approved_events_count = approved_1 + approved_2

    filter_denied_events1 = @denied_events.where('start_date < ?', start_date)
    denied_1 = filter_denied_events1.where('end_date >= ?', start_date).count
    filter_denied_events2 = @denied_events.where('start_date >= ?', start_date)
    denied_2 = filter_denied_events2.where('start_date <= ?', end_date).count
    @denied_events_count = denied_1 + denied_2

    filter_canceled_events1 = @canceled_events.where('start_date < ?', start_date)
    canceled_1 = filter_canceled_events1.where('end_date >= ?', start_date).count
    filter_canceled_events2 = @canceled_events.where('start_date >= ?', start_date)
    canceled_2 = filter_canceled_events2.where('start_date <= ?', end_date).count
    @canceled_events_count = canceled_1 + canceled_2

    filter_pending_events1 = @pending_events.where('start_date < ?', start_date)
    pending_1 = filter_pending_events1.where('end_date >= ?', start_date).count
    filter_pending_events2 = @pending_events.where('start_date >= ?', start_date)
    pending_2 = filter_pending_events2.where('start_date <= ?', end_date).count
    @pending_events_count = pending_1 + pending_2

    if @total_events.present? && @total_events != 0
      @approved = (@approved_events_count.to_f / @total_events.to_f * 100).round(2)
      @denied = (@denied_events_count.to_f / @total_events.to_f * 100).round(2)
      @canceled = (@canceled_events_count.to_f / @total_events.to_f * 100).round(2)
      @pending = (@pending_events_count.to_f / @total_events.to_f * 100).round(2)

    else
      @approved = 0
      @denied = 0
      @canceled = 0
      @pending = 0
    end
    data = {
      total: @total_events,
      approved: @approved,
      denied: @denied,
      canceled: @canceled,
      pending: @pending
    }
    render json: data
  end

  def calendar
    redirect_to root_path if current_user.inactive?
    user = current_user
    @filter_status = params[:filter].presence || 'approved'
    if @filter_status == 'all'
      @events = user.events_by_role
    else
      case @filter_status
      when 'approved'
        status = 3
      when 'denied'
        status = 2
      when 'canceled'
        status = 1
      when 'pending'
        status = 0
      else
        render_404
      end
      @events = user.events_by_role.where(status: status)
    end
    # @events = user.events_by_role
    events = []
    t_z =  ActiveSupport::TimeZone[user.time_zone]
    tz = t_z.tzinfo.identifier

    @events.each do |event|
      item = {}
      %i(id description user_id status start_date end_date category_id).each do |i|
        item[i] = event[i]
        item[:full_name] = event.user.full_name
        item[:event_user_role] = event.user.user_role
        item[:submit_date] = event.created_at
        if event.category_id == 0
          item[:category_name] = 'No category'
        else
          item[:category_name] = event.category_name
        end
      end
      events << item
    end
    gon.events = events
    gon.current_user_role = user.user_role
    gon.time_zone = tz
    gon.current_user_id = current_user.id
    gon.category_first_id = Category.where(default_option: true).first.id
  end
  
  def show
    @event = Event.find(params[:id])
  end

  def create
    description = params[:description]
    category_id = params[:category_id].to_i
    user = current_user
    time_zone = current_user.time_zone

    user_id = params[:creator].to_i
    tz = params[:timezone].to_i

    if category_id == 0
      category_id = Category.find_by(default_option: true).id
    end

    if user_id.present?
      if user_id != 0
        user = User.find(user_id)
      else
        user = current_user
      end

      if tz == 0
        time_zone = current_user.time_zone
      elsif tz == 1
        time_zone = User.find(user_id).time_zone
      else
        time_zone = current_user.time_zone
      end
    end

    t_z =  ActiveSupport::TimeZone[time_zone]
    tz = t_z.tzinfo.identifier

    # initialize entered date by UTC
    start_str =  DateTime.parse(params[:start_date]).strftime('%a, %d %b %Y %H:%M:%S')
    end_str =  DateTime.parse(params[:end_date]).strftime('%a, %d %b %Y %H:%M:%S')
    start_date = DateTime.parse(start_str).utc
    end_date = DateTime.parse(end_str).utc
    # Change timezone only without chaning date, time value  to user timezone
    start_local = start_date.asctime.in_time_zone(time_zone)
    end_local = end_date.asctime.in_time_zone(time_zone)
    
    # Change date time to UTC for saving DB
    start_UTC = start_local.utc
    end_UTC = end_local.utc

    # @holidays = user.get_holidays(start_UTC, end_UTC)

    if user_id.present?
      start_current_user_local = start_UTC.in_time_zone(current_user.time_zone)
      end_current_user_local = end_UTC.in_time_zone(current_user.time_zone)
    else
      start_current_user_local = start_local
      end_current_user_local = end_local
    end

    event = user.events.new(description: description, start_date: start_UTC, end_date: end_UTC, category_id: category_id)

    if event.save
      respond = {
        id: event.id,
        user_name: event.user.full_name,
        user_slug: event.user.slug,
        user_id: current_user.id,
        start_D: Dates.d_Date(start_current_user_local),
        end_D: Dates.d_Date(end_current_user_local),
        event_user_role: event.user.user_role,
        current_role: current_user.user_role,
        time_zone: tz,
        category_id: event.category.id,
        category_name: event.category_name,
        event_off_time: event.event_off_time,
        submite_date: event.created_at.in_time_zone(current_user.time_zone).strftime("%b %d %Y,%l:%M %p")
      }  # used Dates module

      render json: respond

      UserMailer.create_event_notification(user, event, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
      
    else
      render json: event.errors, status: :unprocessable_entity 
    end


  end

  def update
    description = params[:description]
    user = current_user
    time_zone = current_user.time_zone
    category_id = params[:category_id].to_i
    user_id = params[:updater].to_i
    tz = params[:timezone].to_i

    if category_id == 0
      category_id = event.category_id
      return false
    end
    if user_id.present?
      if user_id == 0
        user = event.user
      elsif user_id == -1
        user = current_user
      else
        user = User.find(user_id)
      end
    
      if tz == 0
        time_zone = current_user.time_zone
      elsif tz == 1
        time_zone = User.find(user_id).time_zone
      else
        time_zone = current_user.time_zone
      end
    end

    t_z =  ActiveSupport::TimeZone[time_zone]
    tz = t_z.tzinfo.identifier

    # initialize entered date by UTC
    start_str =  DateTime.parse(params[:start_date]).strftime('%a, %d %b %Y %H:%M:%S')
    end_str =  DateTime.parse(params[:end_date]).strftime('%a, %d %b %Y %H:%M:%S')

    start_date = DateTime.parse(start_str).utc
    end_date = DateTime.parse(end_str).utc
   
    # Change timezone only without chaning date, time value  to user timezone
    start_local = start_date.asctime.in_time_zone(time_zone)
    end_local = end_date.asctime.in_time_zone(time_zone)
    
    # Change date time to UTC for saving DB
    start_UTC = start_local.utc
    end_UTC = end_local.utc

    if user_id.present?
      start_current_user_local = start_UTC.in_time_zone(current_user.time_zone)
      end_current_user_local = end_UTC.in_time_zone(current_user.time_zone)
    else
      start_current_user_local = start_local
      end_current_user_local = end_local
    end

    if (current_user.admin? || event.pending?)

      if (event.update_attributes(description: description, start_date: start_UTC, end_date: end_UTC, user_id: user.id, category_id: category_id))
        respond = {
          start_local: Dates.d_Date(start_current_user_local),
          end_local: Dates.d_Date(end_current_user_local),
          time_zone: tz,
          user_id: event.user.id,
          user_fullname: event.user.full_name,
          user_name_role: event.user.full_name_role,
          event_user_role: user.user_role,
          category_name: event.category_name,
          duration: event.event_off_time.round(2)
        }

        render json: respond
        UserMailer.update_event_notification(event, user, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
      else
        render json: event.errors, status: :unprocessable_entity 
      end
    end
  end

  def get
    user = current_user
    if (user.admin? || user.manager?)
      events = Event.all
    else
      events = user.events.all
    end

    render json: events
  end

  def change
    event_id = params[:event_id]
    status = params[:status]
    updated_status = 0
    user = current_user
    time_zone = user.time_zone
    # initialize entered date by UTC
    start_UTC =  event.start_date
    end_UTC =  event.end_date
    
    if (current_user.admin?)
      if status == '3' #approved
        if (event.update_attributes(status: status, status_by: current_user.id))
          updated_status = 3
          
          UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
        else
          updated_status = 0
        end
      elsif status == '2' #denied
        if (event.update_attributes(status: status, status_by: current_user.id))
          updated_status = 2
          
          UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
        else
          updated_status = 0
        end
      else
        updated_status = 0
      end
    elsif (current_user.manager?)
      updated_status = 0
      if (event.pending?) 
        if (event.user.manager?)
          if status == '1' # manager cancel own event
            if (event.update_attributes(status: status, status_by: current_user.id))
              updated_status = 1
              
              UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
            else
              updated_status = 0
            end
          else
            updated_status = 0
          end
        else
          if status == '3' # manager approve employee event
            if (event.update_attributes(status: status, status_by: current_user.id))
              updated_status = 3
              
              UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
            else
              updated_status = 0
            end
          elsif status == '2' #manager deny employee event
            if (event.update_attributes(status: status, status_by: current_user.id))
              updated_status = 2
              
              UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
            else
              updated_status = 0
            end
          end
        end
      end
    else
      updated_status = 0
      if (event.pending?) 
        if status == '1' # employee cancel own event
          if (event.update_attributes(status: status, status_by: current_user.id))
            updated_status = 1
            
            UserMailer.change_status_event_notification(current_user, event, updated_status, Dates.d_Date(start_UTC), Dates.d_Date(end_UTC)).deliver_later
          else
            updated_status = 0
          end
        end
      end
    end

    user_role = current_user.user_role

    respond = {
      id: event_id,
      updated_status: updated_status,
      changed_by: event.changed_by,
      user_role: user_role
    }
    render json: respond
  end

  def edit
    @event = event
    @time_zone = current_user.time_zone

    gon.time_zone = @time_zone

    start_local = DateTime.parse(@event.start_D).in_time_zone(@time_zone)
    end_local = DateTime.parse(@event.end_D).in_time_zone(@time_zone)
    @start_Local =  Dates.d_Date(start_local)
    @end_Local =  Dates.d_Date(end_local)
    gon.event_start = @start_Local
    gon.event_end = @end_Local
    gon.current_user_role = current_user.user_role
  end

  def comment
    comment = params[:comment]
    comment_status = 0
    user = current_user


    if (current_user.admin? || (current_user.manager? && event.user.employee?))
      if (comment != '' || comment.present?)
        if event.update_attributes(comment: comment)
          comment_status = 1
          left_time = Time.now.strftime("%b %d,%l:%M %p")
        end
      end
      render json: comment_status
      UserMailer.create_comment_notification(current_user, event, left_time, Dates.d_Date(event.start_date), Dates.d_Date(event.end_date)).deliver_later
    end
  end

  def user_events
    redirect_to root_path if !current_user.admin?
    user_id = params[:user_id]
    @user = User.find_by(slug: user_id)

    @filter_status = params[:filter].presence || 'all'
    if @filter_status == 'all'
      @events = @user.events_by_role
    else
      case @filter_status
      when 'approved'
        status = 3
      when 'denied'
        status = 2
      when 'canceled'
        status = 1
      when 'pending'
        status = 0
      else
        render_404

      end
      @events = @user.events_by_role.where(status: status)
    end
    @time_zone = current_user.time_zone
    @category_default_first_id = Category.where(default_option: true).first.id
  end

  def destroy
    if current_user.admin? || (current_user.event_create_perm? && event.pending?)
      event.update_attribute(:current_user, current_user)
      @destroyed_event = event.to_json
      if event.destroy
        UserMailer.delete_event_notification(@destroyed_event, current_user).deliver_later
        render json: {}, status: :ok
      else
      end
    else
    end
  end

  def report
    if current_user.admin?
      @user = current_user
      @time_zone = @user.time_zone
      t_z =  ActiveSupport::TimeZone[@time_zone]
      tz = t_z.tzinfo.identifier
      start_d = params[:start_date]
      end_d = params[:end_date]
      @category_ids = params[:category]

      @reports = Event.all
      
      if @category_ids.present?
        @reports = @reports.where(category_id: @category_ids)
      end

      @reports_approved = @reports.where(status: 3)
      @reports_denied = @reports.where(status: 2)
      @reports_canceled = @reports.where(status: 1)
      @reports_pending = @reports.where(status: 0)

      if start_d.present?
        # initialize entered date by UTC
        start_str = DateTime.parse(start_d).strftime('%a, %d %b %Y %H:%M:%S')
        end_str = DateTime.parse(end_d) + 1.days
        end_str = end_str.strftime('%a, %d %b %Y %H:%M:%S')

        start_date = DateTime.parse(start_str).utc
        end_date = DateTime.parse(end_str).utc
        # Change timezone only without chaning date, time value  to user timezone
        start_local = start_date.asctime.in_time_zone(@time_zone)
        end_local = end_date.asctime.in_time_zone(@time_zone)
        
        # Change date time to UTC for saving DB
        start_UTC = start_local.utc
        end_UTC = end_local.utc
        reports_approved = @reports_approved.where('start_date >= ?', start_UTC).where('start_date <= ?', end_UTC).or(@reports_approved.where('end_date >= ?', start_UTC).where('end_date <= ?', end_UTC)).or(@reports_approved.where('start_date < ?', start_UTC).where('end_date > ?', end_UTC))

        reports_denied = @reports_denied.where('start_date >= ?', start_UTC).where('start_date <= ?', end_UTC).or(@reports_denied.where('end_date >= ?', start_UTC).where('end_date <= ?', end_UTC)).or(@reports_denied.where('start_date < ?', start_UTC).where('end_date > ?', end_UTC))

        reports_canceled = @reports_canceled.where('start_date >= ?', start_UTC).where('start_date <= ?', end_UTC).or(@reports_canceled.where('end_date >= ?', start_UTC).where('end_date <= ?', end_UTC)).or(@reports_canceled.where('start_date < ?', start_UTC).where('end_date > ?', end_UTC))

        reports_pending = @reports_pending.where('start_date >= ?', start_UTC).where('start_date <= ?', end_UTC).or(@reports_pending.where('end_date >= ?', start_UTC).where('end_date <= ?', end_UTC)).or(@reports_pending.where('start_date < ?', start_UTC).where('end_date > ?', end_UTC))

        @reports_approved = reports_approved
        @reports_denied = reports_denied
        @reports_canceled = reports_canceled
        @reports_pending = reports_pending
        gon.str_start = Dates.d_date(start_local)
        gon.str_end = Dates.d_date(end_local - 1.days)

      end
    else
      redirect_to root_path
    end
  end

  def report_filter
    if current_user.admin?

      start_d = params[:start_date]
      end_d = params[:end_date]
      category = params[:category][:id]
      categories = category.reject{|c| c.empty?}

      @reports = Event.all

      if start_d.present?
        if category.present?
          redirect_to report_path(start_date: start_d, end_date: end_d, category: category)
        else
          redirect_to report_path(start_date: start_d, end_date: end_d)
        end
      else
        if category.present?
          redirect_to report_path(category: category)
        else
          redirect_to report_path
        end
      end
    else
      redirect_to root_path
    end
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end


  protected
    def event
      @event ||= Event.find(params[:event_id])
    end

end
