# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    puts "ENV:___#{ENV["LDAP_ATTRIBUTE"]}___"
    super
  end

  # POST /resource/sign_in
  def create
    # @status = "failed"

    # if params[:user][:email].include?('.co')
    #   resource = User.find_for_database_authentication(email: params[:user][:email].downcase)

    # else
    #   resource = User.find_for_database_authentication(username: params[:user][:email].downcase)

    # end

    # if resource.present?
    #   if resource.valid_password?(params[:user][:password])
    #     @status = "success"
    #     sign_in :user, resource
    #     redirect_to root_path
    #   else
    #     @status = "Incorrect Username or Password. Please try again."
    #     redirect_to new_user_session_path, alert: @status

    #   end
    # else
    #   @status = "Incorrect Username or Password. Please try again."
    #   redirect_to new_user_session_path, alert: @status
    # end
    super
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  

  def check_email
    @user = User.find_by_email(params[:user][:email].downcase) || User.find_by_username(params[:user][:email])
    respond_to do |format|
     format.json { render :json => @user.present? }
    end
  end

  def check_email_registration
    if params[:user][:email].present?
      @user = User.find_by_email(params[:user][:email].downcase)
    elsif params[:user][:username]
      @user = User.find_by_username(params[:user][:username])
    end
    respond_to do |format|
     format.json { render :json => !@user.present? }
    end
  end

  def check_username
    @user = User.find_by_username(params[:user][:username])
    @current_user = User.find(params[:id]) if params[:id].present?
    respond_to do |format|
      if @user.present? && @user.id == @current_user.id
        format.json { render :json => @user.present? }
      else
        format.json { render :json => !@user.present? }
      end
    end
  end

  protected

  # If you have extra params to permit, append them to the sanitizer.
    def configure_sign_in_params
      devise_parameter_sanitizer.permit(:sign_in, keys: [:username])
    end

end
