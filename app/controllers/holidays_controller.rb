class HolidaysController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  
  before_action :check_active_status
  before_action :check_permission
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.holiday_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @holiday = Holiday.new
    @holidays = Holiday.all.order(holiday_date: :asc)
  end

  def new
    
  end

  def create
    holiday = Holiday.new holiday_params

    if holiday.save
      flash[:notice] = I18n.t 'new_holiday_success'
    else
      flash[:alert] = I18n.t 'new_holiday_fail'
    end
    redirect_to holidays_path
  end

  def edit
    @holiday = holiday
    @holidays = Holiday.all.order(holiday_date: :asc)
  end

  def update
    if (holiday.update_attributes holiday_params)
      redirect_to edit_holiday_path
      flash[:notice] = I18n.t 'edit_holiday_success'
    else
      redirect_to edit_holiday_path
      flash[:alert] = I18n.t 'edit_holiday_fail'
    end
  end

  def destroy
    if holiday.destroy
      flash[:notice] = I18n.t 'delete_holiday_success'
      redirect_to holidays_path
    else
      flash[:alert] = I18n.t 'delete_holiday_fail'
      redirect_to holidays_path
    end
  end

  protected
    def holiday
      @holiday ||= Holiday.friendly.find(params[:id])
    end

  private
    def holiday_params
      params.require(:holiday).permit(:id, :name, :holiday_date, :holidaytype_id)
    end
end
