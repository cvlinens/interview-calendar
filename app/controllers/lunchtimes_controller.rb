class LunchtimesController < ApplicationController
  before_action :authenticate_user!, except: []
  layout "index"
  include Dates
  before_action :check_active_status
  before_action :check_permission, except: [:show, :render_404]
   
  def check_permission
    redirect_to root_path unless current_user.admin? || current_user.lunchtime_perm?
  end

  def check_active_status
    redirect_to inactive_status_path if current_user.inactive?
  end

  def index
    @lunchtimes = Lunchtime.all.order(name: :asc)
  end

  def new
    name = params[:lunchtime_name]
    lunchtime_from = params[:lunchtime_from]
    lunchtime_to = params[:lunchtime_to]

    lunchtime = Lunchtime.new(name: name, lunchtime_from: lunchtime_from, lunchtime_to: lunchtime_to)

    if lunchtime.save
      flash[:notice] = I18n.t 'new_lunchtime_success'
      redirect_to lunchtimes_path
    else
      redirect_to lunchtimes_path
      flash[:alert] = I18n.t 'new_lunchtime_fail'
    end
  end

  def edit
    @lunchtime = lunchtime
    @lunchtimes = Lunchtime.all.order(name: :asc)
    @user = current_user
    @users = User.where(lunchtime_id: lunchtime.id).order(first_name: :asc)
  end

  def update
    name = params[:lunchtime_name]
    lunchtime_from = params[:lunchtime_from]
    lunchtime_to = params[:lunchtime_to]

    if current_user.admin?
      if (lunchtime.update_attributes(name: name, lunchtime_from: lunchtime_from, lunchtime_to: lunchtime_to))
        redirect_to edit_lunchtime_path
        flash[:notice] = I18n.t 'edit_lunchtime_success'
      else
        redirect_to edit_lunchtime_path
        flash[:alert] = I18n.t 'edit_lunchtime_fail'
      end
    end
  end

  def show

  end

  def destroy
    lunchtime_id = lunchtime.id
    if lunchtime.user.blank?
      if lunchtime.destroy
        users = User.where(lunchtime_id: lunchtime_id)
        users.each do |user|
          if user.update_attribute('lunchtime_id', 0)
            puts "Okay"
          else
            puts "Failed"
          end
        end
        flash[:notice] = I18n.t 'delete_lunchtime_success'
      else
        flash[:alert] = I18n.t 'delete_lunchtime_fail'
      end
    else
      flash[:alert] = I18n.t 'delete_lunchtime_no_empty'
    end
    redirect_to lunchtimes_path
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  protected
    def lunchtime
      @lunchtime ||= Lunchtime.friendly.find(params[:id]) 
    end
end
