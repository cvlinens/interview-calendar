module ApplicationHelper
  def flash_class(level)
    case level
    when "notice" then "alert alert-info"
    when "success" then "alert alert-success"
    when "error" then "alert alert-danger"
    when "alert" then "alert alert-danger"
    end
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "#{flash_class(msg_type)} fade in text-center") do
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat message
            end)
    end
    nil
  end
  
  def resource_name
    :user
  end

  def resource
      @resource ||= User.new
  end

  def resource_class 
      User 
  end

  def devise_mapping
      @devise_mapping ||= Devise.mappings[:user]
  end

end
