/**
 * Resize function without multiple trigger
 *
 * Usage:
 * $(window).smartresize(function(){
 *     // code here
 * });
 */



(function($, sr) {
  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function(func, threshold, execAsap) {
    var timeout;

    return function debounced() {
      var obj = this,
        args = arguments;

      function delayed() {
        if (!execAsap)
          func.apply(obj, args);
        timeout = null;
      }

      if (timeout)
        clearTimeout(timeout);
      else if (execAsap)
        func.apply(obj, args);

      timeout = setTimeout(delayed, threshold || 100);
    };
  };

  // smartresize
  jQuery.fn[sr] = function(fn) {
    return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
  };

})(jQuery, 'smartresize');
/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
  $BODY = $('body'),
  $MENU_TOGGLE = $('#menu_toggle'),
  $SIDEBAR_MENU = $('#sidebar-menu'),
  $SIDEBAR_FOOTER = $('.sidebar-footer'),
  $LEFT_COL = $('.left_col'),
  $RIGHT_COL = $('.right_col'),
  $NAV_MENU = $('.nav_menu'),
  $FOOTER = $('footer');



// Sidebar
function init_sidebar() {
  // TODO: This is some kind of easy fix, maybe we can improve this
  var setContentHeight = function() {
    // reset height
    $RIGHT_COL.css('min-height', $(window).height());

    var bodyHeight = $BODY.outerHeight(),
      footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
      leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
      contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

    // normalize content
    contentHeight -= $NAV_MENU.height() + footerHeight;

    $RIGHT_COL.css('min-height', contentHeight);
  };

  $SIDEBAR_MENU.find('a').on('click', function(ev) {
    console.log('clicked - sidebar_menu');
    var $li = $(this).parent();

    if ($li.is('.active')) {
      $li.removeClass('active active-sm');
      $('ul:first', $li).slideUp(function() {
        setContentHeight();
      });
    } else {
      // prevent closing menu if we are on child menu
      if (!$li.parent().is('.child_menu')) {
        $SIDEBAR_MENU.find('li').removeClass('active active-sm');
        $SIDEBAR_MENU.find('li ul').slideUp();
      } else {
        if ($BODY.is(".nav-sm")) {
          $SIDEBAR_MENU.find("li").removeClass("active active-sm");
          $SIDEBAR_MENU.find("li ul").slideUp();
        }
      }
      $li.addClass('active');

      $('ul:first', $li).slideDown(function() {
        setContentHeight();
      });
    }
  });

  // toggle small or large menu
  $MENU_TOGGLE.on('click', function() {
    console.log('clicked - menu toggle');

    if ($BODY.hasClass('nav-md')) {
      $SIDEBAR_MENU.find('li.active ul').hide();
      $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
    } else {
      $SIDEBAR_MENU.find('li.active-sm ul').show();
      $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
    }

    $BODY.toggleClass('nav-md nav-sm');

    setContentHeight();
  });

  // check active menu
  $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

  $SIDEBAR_MENU.find('a').filter(function() {
    return this.href == CURRENT_URL;
  }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
    setContentHeight();
  }).parent().addClass('active');

  // recompute content when resizing
  $(window).smartresize(function() {
    setContentHeight();
  });

  setContentHeight();

  // fixed sidebar
  if ($.fn.mCustomScrollbar) {
    $('.menu_fixed').mCustomScrollbar({
      autoHideScrollbar: true,
      theme: 'minimal',
      mouseWheel: {
        preventDefault: true
      }
    });
  }
};
// /Sidebar

var randNum = function() {
  return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
};


// Panel toolbox
$(document).ready(function() {

  Cookies.set('time_zone', moment.tz.guess());

  
  $('.collapse-link').on('click', function() {
    var $BOX_PANEL = $(this).closest('.x_panel'),
      $ICON = $(this).find('i'),
      $BOX_CONTENT = $BOX_PANEL.find('.x_content');

    // fix for some div with hardcoded fix class
    if ($BOX_PANEL.attr('style')) {
      $BOX_CONTENT.slideToggle(200, function() {
        $BOX_PANEL.removeAttr('style');
      });
    } else {
      $BOX_CONTENT.slideToggle(200);
      $BOX_PANEL.css('height', 'auto');
    }

    $ICON.toggleClass('fa-chevron-up fa-chevron-down');
  });

  $('.close-link').click(function() {
    var $BOX_PANEL = $(this).closest('.x_panel');

    $BOX_PANEL.remove();
  });
});
// /Panel toolbox

// Tooltip
$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body'
  });
});
// /Tooltip

// Progressbar
if ($(".progress .progress-bar")[0]) {
  $('.progress .progress-bar').progressbar();
}
// /Progressbar

// Switchery
$(document).ready(function() {
  if ($(".js-switch")[0]) {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new Switchery(html, {
        color: '#26B99A'
      });
    });
  }
});
// /Switchery


// iCheck
$(document).ready(function() {
  if ($("input.flat")[0]) {
    $(document).ready(function() {
      $('input.flat').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    });
  }
});
// /iCheck


// Accordion
$(document).ready(function() {
  $(".expand").on("click", function() {
    $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");

    if ($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});

// NProgress
if (typeof NProgress != 'undefined') {
  $(document).ready(function() {
    NProgress.start();
  });

  $(window).load(function() {
    NProgress.done();
  });
}


//hover and retain popover when on popover content
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj) {
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
  var container, timeout;

  originalLeave.call(this, obj);

  if (obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover');
    timeout = self.timeout;
    container.one('mouseenter', function() {
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function() {
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    });
  }
};

$('body').popover({
  selector: '[data-popover]',
  trigger: 'click hover',
  delay: {
    show: 50,
    hide: 400
  }
});


function gd(year, month, day) {
  return new Date(year, month - 1, day).getTime();
}


/* CALENDAR */

function init_calendar() {

  if (typeof($.fn.fullCalendar) === 'undefined') {
    return;
  }

  var user_role = gon.current_user_role;
  var current_user_id = gon.current_user_id;
  var data = gon.events;
  var time_zone = gon.time_zone;
  var events = [];
  var category_default_first_id = gon.category_first_id;

  for (var i  = 0; i < data.length; i++) {
    var event_id = data[i].id;
    var user_fullname = data[i].full_name;
    // var hidden_title = data[i].hidden_title;
    var title = data[i].category_name;
    if (user_role != 'employee') {
      if (user_role != data[i].event_user_role) {
        title = title + ' ( ' + user_fullname + ' )';
      } else {
        title = title + ' (You)';
      }
    } else {
      title = title + ' (You)';
    }
    var user_id = data[i].user_id;

    var description = data[i].description;
    // var start_d = moment.utc(new Date(data[i].start_date)).format("LLLL");
    // var end_d = moment.utc(new Date(data[i].end_date)).format("LLLL");
    // var start_d = moment.tz(new Date(data[i].start_date), time_zone).format("LLLL");
    // var end_d = moment.tz(new Date(data[i].end_date), time_zone).format("LLLL");
    // var start_dd = new Date(data[i].start_date)
    // var end_dd = new Date(data[i].end_date)
    var start_date = moment(data[i].start_date);
    var end_date = moment(data[i].end_date);
    var comment = data[i].comment;
    var status = data[i].status;
    var event_user_role = data[i].event_user_role;
    var category_name = data[i].category_name;
    var category_id = data[i].category_id;
    var submit_date = moment(data[i].submit_date);

    var color = 'Grey';
    switch(status) {
      case 0:
        color = 'Grey'; // pending
        str_status = 'Pending';
        break;
      case 1:
        color = '#BCBD22'; // Canceled
        str_status = 'Canceled';
        break;
      case 2:
        color = '#E48D8A'; // Denied
        str_status = 'Denied';
        break;
      case 3:
        color = 'Green'; // Approved
        str_status = 'Approved';
        break;
      default:
        color = 'Grey'; // pending
        str_status = 'Pending';
    }
    var editable = true;
    if (status !=0) {
        editable = false;
    }
    
    // full calendar

    var validStart = '';
    var validEnd = '';

    var d_start = new Date();
    var d_start_date = d_start.getDate();
    var d_start_Month = d_start.getMonth();
    var d_start_week = d_start.getDay();
    var d_add_date = 0;

    var d_end = new Date();
    var d_end_date = d_end.getDate();
    var d_end_Month = d_end.getMonth();
    var d_end_week = d_end.getDay();

    // date range picker
    var validStart_1 = '';
    var validEnd_1 = '';

    var d_start_1 = new Date();
    var d_start_1_date = d_start_1.getDate();
    var d_start_1_Month = d_start_1.getMonth();
    var d_start_1_week = d_start_1.getDay();

    var d_end_1 = new Date();
    var d_end_1_date = d_end_1.getDate();
    var d_end_1_Month = d_end_1.getMonth();
    var d_end_1_week = d_end_1.getDay();


    if (user_role == 'admin') {
      // full calendar
      validStart = '';
      validEnd = ''
      // date rangepicker
      validStart_1 = false;
      validEnd_1 = false;
    } else if (user_role == 'manager') {
      validStart = new Date();
      validStart.setDate(validStart.getDate() - 1);
      validStart_1 = new Date();
      // full calendar
      d_end.setDate(d_end_date + 1);
      d_end.setHours(23);
      d_end.setMinutes(59);
      d_end.setMonth(d_end_Month + 3);
      
      // date rangepicker
      d_end_1.setDate(d_end_1_date);
      d_end_1.setHours(23);
      d_end_1.setMinutes(59);
      d_end_1.setMonth(d_end_1_Month + 3);

      validEnd = d_end;
      validEnd_1 = d_end_1;
    } else {
      // full calendar
      d_start.setDate(d_start_date + 2);
      d_start.setHours(2);
      d_start.setMinutes(0);
      // date rangepicker
      d_start_1.setDate(d_start_1_date + 3);
      d_start_1.setHours(2);
      d_start_1.setMinutes(0);
      //
      if (d_start.getDay() == 6) {
        d_add_date = 2;
      } else if (d_start.getDay() == 0) {
        d_add_date = 1;
      } else {
        d_add_date = 0;
      }
      d_start.setDate(d_start.getDate() + d_add_date);
      d_start_1.setDate(d_start_1.getDate() + d_add_date);
      validStart = d_start;
      validStart_1 = d_start_1;
      
      // full calendar
      d_end.setDate(d_end_date + 1);
      d_end.setHours(23);
      d_end.setMinutes(59);
      d_end.setMonth(d_end_Month + 3);
      
      // date rangepicker
      d_end_1.setDate(d_end_1_date);
      d_end_1.setHours(23);
      d_end_1.setMinutes(59);
      d_end_1.setMonth(d_end_1_Month + 3);

      validEnd = d_end;
      validEnd_1 = d_end_1;
    }

    var event = {
      title: title,
      description: description,
      user_fullname: user_fullname,
      user_id: user_id,
      start: start_date,
      end: end_date,
      category_name: category_name,
      category_id: category_id,
      color: color,
      status: str_status,
      event_id: event_id,
      submit_date: submit_date,
      event_user_role: event_user_role,
      resourceEditable: editable,
      eventResourceEditable: editable,
      editable: editable
    };
    events.push(event);
  }

  var calendar = $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,listMonth'
      // right: 'month,agendaWeek,agendaDay,listMonth'
    },
    selectable: true,
    selectHelper: true,
    editable: true,
    eventLimit: true,
    navLinks: true,   
    displayEventTime: true,
    displayEventEnd: true,
    eventColor: 'Grey',
    events: events,
    // selectConstraint: {
    //   start: validStart,
    //   end: validEnd
    // },
    validRange: {
      start: validStart,
      end: validEnd
    },
    // timezoneParam: 'timezone',
    // timezone: gon.time_zone,
    select: function(start, end, allDay) {
      // var check = calendar.formatDate(start,'yyyy-MM-dd');
      // var today = calendar.formatDate(new Date(),'yyyy-MM-dd');
      // if(start.isBefore(moment(new Date()) - 2)) {
      //   $('#calendar').fullCalendar('unselect');
      //   return false;
      // }
      $("#descr").val('');
      $('#fc_create').click();
      $('#category_id').val(category_default_first_id);
      $('#creator_id').val(current_user_id);

      var endDate = new Date(end);

      beforeDay = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(), endDate.getHours() - 1, 59, 59);
      var end_date = moment.tz(beforeDay, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc")
      $('input[name="datetimes3"]').daterangepicker({
        timePicker: true,
        minDate: validStart_1,
        maxDate: validEnd_1,
        startDate: start,
        endDate: end_date,
        locale: {
          format: 'MMMM DD, YYYY hh:mm A'
        }
      });

      $(".antosubmit").off("click");
      $(".antosubmit").on("click", function() {
        var description = $("#descr").val();
        var creator = $('#creator_id').val();
        var tz = $('#creator_tz').val();
        var category_id = $('#category_id').val();
        var d_time = $('#date-ranger3').val();

        var str_start = d_time.split(" - ")[0];
        var str_end = d_time.split(" - ")[1];
        
        // var start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
        // var end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");

        var creator_id = '';
        if (creator) {
          creator_id = creator;
        }
        else {
          creator_id = 0;
        }
        $('.error_event').addClass('hide');

        if(!category_id) {
          var error_notification_html = '';
          error_notification_html = 
            '<div class="alert alert-danger fade in text-center error_event"><button class="close" data-dismiss="alert">x</button>' +
              'No choosen category. Please select category.' +
            '</div>';
          $('.main_container > .right_col').prepend(error_notification_html);

          calendar.fullCalendar('unselect');

        $('.antoclose').click();
          return false;
        }

        var d_data = {
          description: description,
          start_date: str_start,
          end_date: str_end,
          creator: creator_id,
          timezone: tz,
          category_id: category_id
        }

        $.ajax({
          url: "/create_event",
          type: "post",
          data: d_data,
          success: function(result, response) {
            console.log("Created successfully!");
            // calendar.fullCalendar('refetchEvents');
            if (creator_id != "") {
              if (creator_id != result.user_id) {
                title = result.category_name + ' ( ' + result.user_name + ' )';
              }
            }
            if ( tz == 0 ) {
              var start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
              var end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
            }
            else {
              var start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', result.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
              var end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', result.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");

            }
            var submit_date = moment(result.submit_date);
            calendar.fullCalendar('renderEvent', {
                title: title,
                description: description,
                start: moment(start_date),
                end: moment(end_date),
                event_id: result.id,
                user_id: creator_id,
                category_id: category_id,
                submit_date: submit_date
              },
              true // make the event "stick"
            );
            submit_date = moment.tz(submit_date, 'MMMM DD, YYYY hh:mm A', gon.time_zone).format("LLL");
            $('#submit_date').html(submit_date)
          },
          error: function(respond) {
            console.log("Creation is Failed");
            var notification_text = JSON.parse(respond.responseText).title[0]
            var error_notification_html = '';
            error_notification_html = 
            '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
              notification_text +
            '</div>';
            $('.main_container > .right_col').prepend(error_notification_html);
          }
        });

        calendar.fullCalendar('unselect');

        $('.antoclose').click();

        return false;
      });
    },
    eventClick: function(calEvent, jsEvent, view) {
      if (calEvent.editable == false) {
        $('#fc_view').click();
        $("#descr3").val(calEvent.description);
        $('#category_name').val(calEvent.category_name);
        $('#employee_name').val(calEvent.user_fullname);
        
        $('input[name="datetimes4"]').daterangepicker({
          timePicker: false,
          datePicker: false,
          startDate:calEvent.start,
          endDate: calEvent.end,
          locale: {
            format: 'MMMM DD, YYYY hh:mm A'
          }
        });
        var submit_date = moment.tz(calEvent.submit_date, 'MMMM DD, YYYY hh:mm A', gon.time_zone).format("LLL");
  
        $('#submit_date2').html(submit_date)

        return false;
      }

      $('#fc_edit').click();
      $("#descr2").val(calEvent.description);
      $('#category_update_id').val(calEvent.category_id);
      $('#updater_id').val(calEvent.user_id);

      var submit_date = moment.tz(calEvent.submit_date, 'MMMM DD, YYYY hh:mm A', gon.time_zone).format("LLL");

      $('#submit_date').html(submit_date)

      if (calEvent.category_id == 0) {
        $('#category_update_id').val('');
      }

      $('input[name="datetimes2"]').daterangepicker({
        timePicker: true,
        startDate:calEvent.start,
        endDate: calEvent.end,
        minDate: validStart_1,
        maxDate: validEnd_1,
        locale: {
          format: 'MMMM DD, YYYY hh:mm A'
        }
      });
      $(".antosubmit2").off('click');
      $(".antosubmit2").on("click", function() {
        category_name = calEvent.category_name
        
        calEvent.description = $("#descr2").val();
        var updater = $('#updater_id').val();
        var category_id = $('#category_update_id').val();
        var tz = $('#updater_tz').val();

        var d_time = $('#date-ranger2').val();

        var str_start = d_time.split(" - ")[0];
        var str_end = d_time.split(" - ")[1];
        
        var start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
        var end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");

        $('.error_event').addClass('hide');
        var category__id = $('#category_update_id').val();
        if(typeof category__id === 'undefined' || category__id <= 0 || category__id == null ) {
          var error_notification_html = '';
          error_notification_html = 
            '<div class="alert alert-danger fade in text-center error_event"><button class="close" data-dismiss="alert">x</button>' +
              'No choosen category. Please select category.' +
            '</div>';
          $('.main_container > .right_col').prepend(error_notification_html);
          $('.antoclose2').click();
          calendar.fullCalendar('unselect');
          return false;
        }
        var updater_id = '';

        if (updater) {
          updater_id = updater;
        }
        else {
          updater_id = -1;
        }

        var updated_data = {
          description: calEvent.description,
          start_date: str_start,
          end_date: str_end,
          event_id: calEvent.event_id,
          updater: updater_id,
          timezone: tz,
          category_id: category_id
        }
        $.ajax({
          url: "/update_event",
          type: "post",
          data: updated_data,
          success: function(result) {
            console.log("Updated successfully! - Edited");
            if ( tz == 0 ) {
              start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
              end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
            }
            else {
              start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', result.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
              end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', result.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
            }
            category_name = result.category_name;
            if (result.user_fullname) {
              if ((user_role == 'employee')) {
                calEvent.title = category_name
              } else {
                if ((result.event_user_role == 'employee' && user_role == 'admin') || (result.event_user_role == 'employee' && user_role == 'manager') || (result.event_user_role == 'manager' && user_role == 'admin')) {
                  calEvent.title = category_name + " ( " + result.user_fullname + " )";
                }
                else {
                  calEvent.title = category_name + "(You)";
                }
              }
            }
            else {
              calEvent.title = category_name;
            }
            calEvent.start = moment(start_date);
            calEvent.end = moment(end_date);
            calEvent.category_id = category_id;
            calEvent.category_name = category_name;
            calEvent.user_id = result.user_id;
            calendar.fullCalendar('updateEvent', calEvent);
            
          },
          error: function(respond) {
            console.log("No updated - Edit");
            var notification_text = JSON.parse(respond.responseText).title[0]
            var error_notification_html = '';
            error_notification_html = 
            '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
              notification_text +
            '</div>';
            $('.main_container > .right_col').prepend(error_notification_html);
          }
        });
        $('.antoclose2').click();
      });
      
      calendar.fullCalendar('unselect');
      
      
    },
    eventRender: function(eventObj, $el) {
      if (!eventObj.status)  {
        eventObj.status = 'Pending';
      }
      var f_title = eventObj.title + " - " + eventObj.status;
      // if (user_role == 'employee') {
      // }
      // else {
      //   var f_title = eventObj.title + " (" + eventObj.user_fullname + ")";
      // }
      $el.popover({
        title: f_title,
        content: eventObj.description,
        status: eventObj.status,
        trigger: 'hover',
        placement: 'top',
        container: 'body'
      });
    },
    eventDrop: function(event, delta, revertFunc) {
      var split_key = " ( " + event.user_fullname + " )";
      var str_start = moment(event.start).format("LLL");
      var str_end = moment(event.end).format("LLL");
      var category_id = event.category_id;
      var updater = event.user_id;
      var updated_data = {
        description: event.description,
        start_date: str_start,
        end_date: str_end,
        event_id: event.event_id,
        category_id: category_id,
        updater: updater
      }
      $('.error_event').addClass('hide');
      if(category_id == 0 || !category_id) {
        var error_notification_html = '';
        error_notification_html = 
          '<div class="alert alert-danger fade in text-center error_event"><button class="close" data-dismiss="alert">x</button>' +
            'No choosen category. Please select category.' +
          '</div>';
        $('.main_container > .right_col').prepend(error_notification_html);
        return false;
      }
      $.ajax({
        url: "/update_event",
        type: "post",
        data: updated_data,
        success: function(result) {
          console.log("Updated successfully! - moved");
          var start_date = moment.tz(str_start, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
          var end_date = moment.tz(str_end, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ");
          event.start = moment(start_date);
          event.end = moment(end_date);
          calendar.fullCalendar('updateEvent', event);
        },
        error: function(respond) {
          var notification_text = JSON.parse(respond.responseText).title[0]
          var error_notification_html = '';
          error_notification_html = 
          '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
            notification_text +
          '</div>';
          $('.main_container > .right_col').prepend(error_notification_html);
        }
      });
    },
    eventResize: function(event, delta, revertFunc) {
      var split_key = " ( " + event.user_fullname + " )";
      
      var updated_data = {
        description: event.description,
        start_date: event.start._d,
        end_date: event.end._d,
        event_id: event.event_id
      }
      
      $.ajax({
        url: "/update_event",
        type: "post",
        data: updated_data,
        success: function() {
          console.log("Updated successfully! - resized");
        },
        error: function(respond) {
          console.log("No updated - resized");
          var notification_text = JSON.parse(respond.responseText).title[0]
          var error_notification_html = '';
          error_notification_html = 
          '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
            notification_text +
          '</div>';
          $('.main_container > .right_col').prepend(error_notification_html);
        }
      });
    }
  });
};

$(document).ready(function() {
  var toggler = document.getElementsByClassName("c-caret");
  var i;

  for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function() {
      this.parentElement.querySelector(".n-nested").classList.toggle("activated");
      this.classList.toggle("c-caret-down");
    });
  }

  //--------Select2 initialize---------
  $('#department_multiples').select2({
    width: 'calc(100% - 200px)'
  });
  $('#report_cateogry').select2({
    width: '100%'
  });
  $('#parent_department').select2ToTree({
    width: '100%'
  });
  $('#user_department_ids').select2ToTree({
    width: 'calc(100% - 200px)'
  });

  // --------DataTable initialize----------
  $('#userTable').DataTable();
  $('#departmentUserTable').DataTable();
  $('#eventTable').DataTable({
    "order": [[5, "asc"]] // sort by submitted date
  });
  $('#userEventTable').DataTable({
    "order": [[5, "asc"]] // sort by submitted date
  });
  $('#categoryEventTable').DataTable({
    "order": [[5, "asc"]] // sort by submitted date
  });
  $('#approvedReportUsersTable').DataTable({
    "order": [[6, "asc"]] // sort by submitted date
  });
  $('#pendingReportUsersTable').DataTable({
    "order": [[6, "asc"]] // sort by submitted date
  });
  $('#deniedReportUsersTable').DataTable({
    "order": [[6, "asc"]] // sort by submitted date
  });
  $('#canceledReportUsersTable').DataTable({
    "order": [[6, "asc"]] // sort by submitted date
  });

  if (gon.events) {
    var default_timezone = gon.time_zone;
    if (default_timezone) {
      moment.tz.setDefault(default_timezone);
    }
    init_calendar();
    $('[data-toggle="tooltip"]').tooltip();
  }

  /*
  var departmentTable = $('#departmentTable').DataTable({
    "order": [[1, 'asc']],
    "searching": false,
    "lengthChange": false
  })
  
  $('#departmentTable').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = departmentTable.row( tr );
    var department_id = $(this).data('id');

    if ( row.child.isShown() ) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {
      var data = {
        id: department_id
      }
      $.ajax({
        type: "post",
        url: "/call_sub",
        data: data,
        success: function(result) {
          // Open this row
          row.child(format_sub(result)).show();
          tr.addClass('shown');
        }
      });
    }
  });

  var selectedParentId = $('#departmentTable').attr('data-selected-parent');
  if(selectedParentId) {
    $('#departmentTable .details-control[data-id="' + selectedParentId + '"]').trigger('click');
  }
  */

  
});

/*
function format_sub(d) {
  var str = window.location.href;
  var k = /\/(\d+)\//.exec(str);
  var html1 = '';
  var html = '<table cellpadding="5" cellspacing="0"  style="padding-left: 50px;" >' +
    '<tr>' +
      '<th>Sub Department Name</th>' +
      '<th>Action</th>' +
    '</tr>';
    for(i = 0; i < d.length; i++) {
      if (k) {
        if (k[1] == d[i].id) {
          html1 = '<tr class="active">';
        } else {
          html1 = '<tr>';
        }
      } else {
        html1 = '<tr>';
      }
      html += 
        html1 + 
          '<td>' + 
            '<a class="department-link" href="/departments/' + d[i].id + '/edit">' +
              d[i].name + 
            '</a>' +
          '</td>' + 
          '<td>' + 
            '<a data-confirm="Are you sure?" rel="nofollow" data-method="delete" href="/departments/' + d[i].id + '">Delete</a>' +
          '</td>' + 
        '</tr>';
    }
    html += '</table>';

    return html;
}
*/


$(document).ready(function() {

  //event page: approve, deny, cancel 
  // Bind to modal opening to set necessary data properties to be used to make request
  $('#confirm-event').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
    $(this).find('.btn-ok').attr('status', $(e.relatedTarget).data('status'));
    $(this).find('.btn-ok').attr('category', $(e.relatedTarget).data('category'));
    var data = $(e.relatedTarget).data();
    $('.btn-ok', this).data('id', data.id);
    $('.btn-ok', this).data('status', data.status);
    $('.btn-ok', this).data('category', data.category);
    $('.btn-ok', this).removeClass('btn-success');
    $('.btn-ok', this).removeClass('btn-danger');
    $('.btn-ok', this).removeClass('btn-warning');
    $('.btn-ok', this).removeClass('btn-basic');
    var confirmLabel = '';
    if (data.status == 3) {
      confirmLabel = "Confirm - Approve";
      lastLabel = "You are about to approve one event."
      btnLabel = "Approve";
      btnStyle = 'btn-success';
    } else if (data.status == 2) {
      confirmLabel = "Confirm - Deny";
      lastLabel = "You are about to deny one event."
      btnLabel = "Deny";
      btnStyle = 'btn-danger';
    } else if (data.status == 1) {
      confirmLabel = "Confirm - Cancel";
      lastLabel = "You are about to cancel one event."
      btnLabel = "Cancel";
      btnStyle = 'btn-warning';
    } else {
      confirmLabel = "Error";
      btnLabel = "Pending";
      lastLabel = "No change will happen."
      btnStyle = 'btn-basic';
    }
    $('h4#myModalEventLabel').html(confirmLabel);
    $('.btn-eventConfirm').html(btnLabel);
    $('.last-label').html(lastLabel);
    
    $('.btn-ok', this).addClass(btnStyle);

  });

  // Bind click to OK button within popup
  $('#confirm-event').on('click', '.btn-ok', function(e) {

    var $modalDiv = $(e.delegateTarget);
    $modalDiv.addClass('processing');
    
    var event_id = $(this).data('id');
    var status = $(this).data('status');
    var category = $(this).data('category');
    $('.error_event').addClass('hide');
    if(!category) {
      var error_notification_html = '';
      error_notification_html = 
        '<div class="alert alert-danger fade in text-center error_event"><button class="close" data-dismiss="alert">x</button>' +
          'No choosen category. Please select category.' +
        '</div>';
      $('.main_container > .right_col').prepend(error_notification_html);

      return false;
    }
    var data = {
      event_id: event_id,
      status: status
    }
    $.ajax({
      type: "post",
      url: "/change",
      data: data,
      success: function(result) {
        setTimeout(function() {
          $modalDiv.modal('hide').removeClass('processing');
        }, 100);
        console.log("Ok. updated");
        var status_html = '';
        var marker_html = '';
        if (result.updated_status == 3) {
          if (result.user_role == 'admin') {
            //event page
            $('[data-id="' + result.id + '"][data-status="3"]').removeClass("btn-basic");
            // $('[data-id="' + result.id + '"][data-status="3"]').prop("disabled", true);
            $('[data-id="' + result.id + '"][data-status="3"]').addClass("btn-success btn-changed");
            $('[data-id="' + result.id + '"][data-status="2"]').html("<span>Deny</span>");
            $('[data-id="' + result.id + '"][data-status="2"]').removeClass("btn-danger btn-basic btn-changed");
            $('[data-id="' + result.id + '"][data-status="3"]').html("<span>Approved</span>");
            //event detail page
            $('.check_status').html("<span>Status:</span>&nbsp;<span>approved</span>");
            marker_html = 
            '<span class="fc-event-dot event-approved" data-toggle="tooltip" title="" data-original-title="approved"></span>';
            $('.check_status_marker').html(marker_html);
          }
          else {
            //event page
            status_html = 
            '<div class="col-md-12 col-sm-12">' +
              '<span class="event_status btn btn-success" disabled data-toggle="tooltip" title="Already approved. Only admin can change this state.">Approved</span>' +
            '</div>';
            $('[data-id="' + result.id + '"][data-status="3"]').closest('.row').html(status_html);

            $('.delete_event[data-id="' + result.id + '"]').closest('.row').html('');
            //event detail page
            $('.check_status').html("<span>Status:</span>&nbsp;<span>approved</span>");
            marker_html = 
            '<span class="fc-event-dot event-approved" data-toggle="tooltip" title="" data-original-title="approved"></span>';
            $('.check_status_marker').html(marker_html);
            $('.edit--event').html('');
          }
          $('.show_status').html('This event was approved by <b>' + result.changed_by + '</b>');
        }
        else if (result.updated_status == 2) {
          if (result.user_role == 'admin') {
            //event page
            $('[data-id="' + result.id + '"][data-status="2"]').removeClass("btn-basic");
            $('[data-id="' + result.id + '"][data-status="2"]').addClass("btn-danger btn-changed");
            $('[data-id="' + result.id + '"][data-status="3"]').html("<span>Approve</span>");
            $('[data-id="' + result.id + '"][data-status="3"]').removeClass("btn-success btn-basic btn-changed");
            $('[data-id="' + result.id + '"][data-status="2"]').html("<span>Denied</span>");
            //event detail page
            $('.check_status').html("<span>Status:</span>&nbsp;<span>denied</span>");
            marker_html = 
            '<span class="fc-event-dot event-denied" data-toggle="tooltip" title="" data-original-title="denied"></span>';
            $('.check_status_marker').html(marker_html);
          }
          else {
            //event page
            status_html = 
            '<div class="col-md-12 col-sm-12">' +
              '<span class="event_status btn btn-danger" disabled data-toggle="tooltip" title="Already denied. Only admin can change this state.">Denied</span>' +
            '</div>';
            $('[data-id="' + result.id + '"][data-status="2"]').closest('.row').html(status_html);
            $('.delete_event[data-id="' + result.id + '"]').closest('.row').html('');

            //event detail page
            $('.check_status').html("<span>Status:</span>&nbsp;<span>denied</span>");
            marker_html = 
            '<span class="fc-event-dot event-denied" data-toggle="tooltip" title="" data-original-title="denied"></span>';
            $('.check_status_marker').html(marker_html);
            $('.edit--event').html('');

          }
          $('.show_status').html('This event was denied by <b>' + result.changed_by + '</b>');
        }
        else if (result.updated_status == 1) {
          //event page
          status_html = 
            '<div class="col-md-12 col-sm-12">' +
              '<span class="event_status btn btn-warning" disabled data-toggle="tooltip" title="Canceled by you.">Canceled</span>' +
            '</div>';
            $('[data-id="' + result.id + '"][data-status="1"]').closest('.row').html(status_html);
            $('.check_status').html("<span>Status:</span>&nbsp;<span>canceled</span>");
            $('.delete_event[data-id="' + result.id + '"]').closest('.row').html('');
            //event detail page
            marker_html = 
            '<span class="fc-event-dot event-canceled" data-toggle="tooltip" title="" data-original-title="canceled"></span>';
            $('.check_status_marker').html(marker_html);
            $('.edit--event').html('');
            $('.show_status').html('This event was canceled by <b>' + result.changed_by + '</b>');
        }
        else {
          console.log("None changed");
        }
      },
      error: function() {

      }
    })
  });

  // Bind to modal opening to set necessary data properties to be used to make request
  $('#confirm-delete').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('id', $(e.relatedTarget).data('id'));
    var data = $(e.relatedTarget).data();
    $('.btn-ok', this).data('id', data.id);

  });

  // Bind click to OK button within popup
  $('#confirm-delete').on('click', '.btn-ok', function(e) {

    var $modalDiv = $(e.delegateTarget);
    $modalDiv.addClass('deleting');
    
    var id = $(this).data('id');
    var data = {
      event_id: id,
    }
    $.ajax({
      type: "delete",
      url: "/delete_event",
      data: data,
      success: function(result) {
        setTimeout(function() {
          $modalDiv.modal('hide').removeClass('deleting');
        }, 100);
        // event page
        $('button.delete_event[data-id="' + id + '"]').closest('tr').remove();
        var delete_notification_html = '';
          delete_notification_html = 
          '<div class="alert alert-danger fade in text-center delete_limit_event"><button class="close" data-dismiss="alert">x</button>' +
            'You deleted the event successfully' +
          '</div>';
          $('.main_container > .right_col').prepend(delete_notification_html);
          // event detail page
          window.location.replace("/events");   // redirect to event page after event is deleted.
      },
      error: function() {

      }
    })
  });

  //event detail page: comment

  $('.fc-list-comment').on('click', 'button.btn-comment', function() { //event bining for dynamic element
    var event_id = $(this).data('id');
    var comment = $(this).siblings("#comment").val();
    if (!comment || comment != undefined  || comment !="") {
      var data = {
        event_id: event_id,
        comment: comment
      }
      $.ajax({
        type: "post",
        url: "/comment",
        data: data,
        success: function(result) {
          var status_html = '';
          if (result == 1) {
            status_html = 
            '<td class="fc-list-item-comment fc-widget-content" colspan="3">' + 
              '<div class="s_desc">' + 
                '<label class="comment_label">Comment:</label>' + 
                '<div class="comment">' + comment + '</div>' +
                '<button type="button" class="btn-edit-comment" data-id="' + event_id + '">Edit comment</button>' +
              '</div>' +
            '</td>';
            $('.btn-comment').closest('.fc-list-comment').html(status_html);
          }
          else {
            console.log("none changed");
          }
        },
        error: function() {
          console.log("no comment");
        }
      });

    }
  });
  

  $('.fc-list-comment').on('click', 'button.btn-edit-comment, button.btn-add-comment', function() { //event bining for dynamic element
    var event_id = $(this).data('id');
    var comment = $(this).siblings(".comment").text();
    var status_html = '';
    status_html = 
      '<td class="fc-list-item-comment-input fc-widget-content" colspan="3">' +
        '<div class="s_desc">' +
          '<label for="comment">Please leave your comment:</label>' +
          '<textarea type="text" id="comment" name="comment" rows="5">' + comment + '</textarea>' +
          '<button type="button" class="btn btn-default btn-comment" data-id="' + event_id + '">Submit</button>' +
        '</div>' +
      '</td>';
    $(this).closest('.fc-list-comment').html(status_html);
  });

  // add event on event page
  $('#EventNew').on('click', 'button.add_event', function() {
    var description = $('#descr').val();
    var creator = $('#creator_id').val();
    var tz = $('#creator_tz').val();
    var d_time = $('#date-ranger').val();
    var category_id = $('#category_id').val();
    var start_date = d_time.split(" - ")[0];
    var end_date = d_time.split(" - ")[1];
    var creator_id = '';
    if (creator) {
      creator_id = creator;
    }
    else {
      creator_id = 0;
    }

    if (!category_id) {
      category_id = 0;
    }

    var data = {
      description: description,
      start_date: start_date,
      end_date: end_date,
      creator: creator_id,
      timezone: tz,
      category_id: category_id
    }

    $.ajax({
      type: "post",
      url: "/create_event",
      data: data,
      success: function(result) {
        $('#descr').val('');
        $('#date-ranger').val('');
        var new_event_html = '';
        var new_event_html1 = '';
        var new_event_html2 = '';
        new_event_html1 = 
        '<tr>' +
        '<td>' +
            '<a class="event_link" href="/events/' + result.slug + '">' +
              '<span>' + result.category_name + '</span>' +
            '</a>' +
          '</td>' +
          '<td>' +
            '<a class="event_link" href="/events/' + result.slug + '">' +
              '<span>' + description + '</span>' +
            '</a>' +
          '</td>' +
          '<td>' +
            '<a class="event_link" href="/events/user_events/' + result.user_slug + '">' +
              '<span>' + result.user_name + '</span>' +
            '</a>' +
          '</td>' +
          '<td>' +
            '<div><span>' + result.start_D + '</span></div>' +
          '</td>' +
          '<td>' +
            '<div><span>' + result.end_D + '</span></div>' +
          '</td>' +
          '<td>' +
            '<div><span>' + result.submite_date + '</span></div>' +
          '</td>' +
          '<td>' +
            '<a class="event_link" href="/events/' + result.id + '">' +
              '<span>' + result.event_off_time + '</span>' +
            '</a>' +
          '</td>';  // not ended <html> <td> tag: it will be continue with below condition
          if (result.current_role == "manager") {
            if (result.event_user_role == "manager") {
              new_event_html2 = 
              '<td>' +
                '<div class="row">' +
                  '<div class="col-md-12 col-sm-12">' +
                    '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="1"><span>Cancel</span></button>' +
                  '</div>' +
                '</div>' +
              '</td>' +
              '<td>' +
                '<div class="row">' +
                  '<div class="col-md-12 col-sm-12">' +
                      '<button class="btn btn-basic delete_event" data-toggle="modal" data-target="#confirm-delete" data-id="' + result.id + '"><span>Delete</span></button>' +
                    '</div>' +
                '</div>' +
              '</td>' +
            '</tr>';
            }
            else {
              new_event_html2 = 
              '<td>' +
                '<div class="row">' +
                  '<div class="col-md-6 col-sm-6">' +
                    '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="3" data-category="' + result.category_id + '"><span>Approve</span></button>' +
                  '</div>' +
                  '<div class="col-md-6 col-sm-6">' +
                    '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="2" data-category="' + result.category_id + '"><span>Deny</span></button>' +
                  '</div>' +
                '</div>' +
              '</td>' +
              '<td>' +
                '<div class="row">' +
                  '<div class="col-md-12 col-sm-12">' +
                      '<button class="btn btn-basic delete_event" data-toggle="modal" data-target="#confirm-delete" data-id="' + result.id + '"><span>Delete</span></button>' +
                    '</div>' +
                '</div>' +
              '</td>' +
            '</tr>';
            }
          }
          else if (result.current_role == "employee") {
            new_event_html2 = 
            '<td>' +
              '<div class="row">' +
                '<div class="col-md-12 col-sm-12">' +
                  '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="1"><span>Cancel</span></button>' +
                '</div>' +
              '</div>' +
            '</td>' +
            '<td>' +
              '<div class="row">' +
                '<div class="col-md-12 col-sm-12">' +
                    '<button class="btn btn-basic delete_event" data-toggle="modal" data-target="#confirm-delete" data-id="' + result.id + '"><span>Delete</span></button>' +
                  '</div>' +
              '</div>' +
            '</td>' +
          '</tr>';
          }
          else {
            new_event_html2 = 
            '<td>' +
              '<div class="row">' +
                '<div class="col-md-6 col-sm-6">' +
                  '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="3" data-category="' + result.category_id + '"><span>Approve</span></button>' +
                '</div>' +
                '<div class="col-md-6 col-sm-6">' +
                  '<button class="btn btn-basic change_status" data-id="' + result.id + '" data-status="2" data-category="' + result.category_id + '"><span>Deny</span></button>' +
                '</div>' +
              '</div>' +
            '</td>' +
            '<td>' +
              '<div class="row">' +
                '<div class="col-md-12 col-sm-12">' +
                    '<button class="btn btn-basic delete_event" data-toggle="modal" data-target="#confirm-delete" data-id="' + result.id + '"><span>Delete</span></button>' +
                  '</div>' +
              '</div>' +
            '</td>' +
          '</tr>';
          }
        new_event_html = new_event_html1 + new_event_html2;
        $('#eventTable tbody').append(new_event_html);

      },
      error: function(respond) {
        console.log("fail modal");
        var notification_text = JSON.parse(respond.responseText).title[0]
        var error_notification_html = '';
        error_notification_html = 
        '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
          notification_text +
        '</div>';
        $('.main_container > .right_col').prepend(error_notification_html);
      }
    })

    $('.antoclose').click();
  });

  $('#EventEdit').on('click', 'button.edit_event2', function() {
    var description = $('#descr').val();
    var event_id = $(this).data("id");
    var category_id = $('#category_update_id').val();
    var updater = $('#updater_id').val();
    var tz = $('#updater_tz').val();
    var d_time = $('#date-ranger').val();

    var start_date = d_time.split(" - ")[0];
    var end_date = d_time.split(" - ")[1];
    var updater_id = '';

    $('.error_event').addClass('hide');
    if (!category_id) {
      category_id = 0;
      var error_notification_html = '';
      error_notification_html = 
        '<div class="alert alert-danger fade in text-center error_event"><button class="close" data-dismiss="alert">x</button>' +
          'No choosen category. Please select category.' +
        '</div>';
      $('.main_container > .right_col').prepend(error_notification_html);
      $('.antoclose').click();

      return false;
    }

    if (updater) {
      updater_id = updater;
    }
    else {
      updater_id = 0;
    }

    var data = {
      description: description,
      start_date: start_date,
      end_date: end_date,
      event_id: event_id,
      updater: updater_id,
      timezone: tz,
      category_id: category_id
    }

    $.ajax({
      type: "post",
      url: "/update_event",
      data: data,
      success: function(result) {
        $('.detail-dashboard .fc-list-item-category').html('<div>' + result.category_name + '</div>');
        $('.detail-dashboard .fc-list-item-description').html('<div class="s_desc">' + description + '</div>');
        $('.detail-dashboard .fc-list-heading-alt').html(result.start_local + '  	&mdash;  ' + result.end_local  +' (' + result.duration + ' hours)');
        $('.detail-dashboard .fc-list-heading-main .created_by').html('<b>' + result.user_name_role + '</b>');
      },
      error: function(respond) {
        var notification_text = JSON.parse(respond.responseText).title[0]
        var error_notification_html = '';
        error_notification_html = 
        '<div class="alert alert-danger fade in text-center error_limit_event"><button class="close" data-dismiss="alert">x</button>' +
          notification_text +
        '</div>';
        $('.main_container > .right_col').prepend(error_notification_html);
      }

    });
    $('.antoclose').click();

  });

  $('#event-filter-option, #event-filter-option-calendar, #user-event-filter-option').change(function() {
    var str_status = $(this).val();
    var status = 0;
    switch(str_status) {
      case "all":
        status = 4;
        break;
      case "approved":
        status = 3;
        break;
      case "denied":
        status = 2;
        break;
      case "pending":
        status = 0;
        break;
      case "canceled":
        status = 1;
        break;
      default:
        status = 4;
    }
    
    if (window.location.href.indexOf("?filter=") >= 0) {
      window.location.href = window.location.href.split("?filter=")[0] + '?filter=' + str_status;
    }
    else {
      window.location.href = window.location.href + '?filter=' + str_status;
    }

  });

  $('#event-filter-option-summary').change(function() {
    var str_filter = $(this).val();
    var filter = 0;
    switch(str_filter) {
      case "all":
        filter = 0;
        break;
      case "last-month":
        filter = 2;
        break;
      case "this-month":
        filter = 1;
        break;
      default:
        filter = 0;
    }

    var data = {
      filter: filter

    }
    $.ajax({
      type: "post",
      url: "/summary_filter",
      data: data,
      success: function(result) {
        var widget__summary_html = '';
        var approved_html = '';
        var denied_html = '';
        var canceled_html = '';
        var pending_html = '';

        approved_html = 
        '<div class="widget_summary">' +
          '<div class="w_left w_25">' +
            '<span>Approved</span>' +
          '</div>' +
          '<div class="w_center w_55">' +
            '<div class="progress">' +
              '<div class="progress-bar cb_approved" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + result.approved + '%;">' +
                '<span class="sr-only">' + result.approved + '%</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="w_right w_20">' +
            '<span>' + result.approved + '%</span>' +
          '</div>' +
          '<div class="clearfix"></div>' +
        '</div>';

        denied_html = 
        '<div class="widget_summary">' +
          '<div class="w_left w_25">' +
            '<span>Denied</span>' +
          '</div>' +
          '<div class="w_center w_55">' +
            '<div class="progress">' +
              '<div class="progress-bar cb_denied" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + result.denied + '%;">' +
                '<span class="sr-only">' + result.denied + '%</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="w_right w_20">' +
            '<span>' + result.denied + '%</span>' +
          '</div>' +
          '<div class="clearfix"></div>' +
        '</div>';

        canceled_html = 
        '<div class="widget_summary">' +
          '<div class="w_left w_25">' +
            '<span>Canceled</span>' +
          '</div>' +
          '<div class="w_center w_55">' +
            '<div class="progress">' +
              '<div class="progress-bar cb_canceled" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + result.canceled + '%;">' +
                '<span class="sr-only">' + result.canceled + '%</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="w_right w_20">' +
            '<span>' + result.canceled + '%</span>' +
          '</div>' +
          '<div class="clearfix"></div>' +
        '</div>';

        pending_html = 
        '<div class="widget_summary">' +
          '<div class="w_left w_25">' +
            '<span>Pending</span>' +
          '</div>' +
          '<div class="w_center w_55">' +
            '<div class="progress">' +
              '<div class="progress-bar cb_pending" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + result.pending + '%;">' +
                '<span class="sr-only">' + result.pending + '%</span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div class="w_right w_20">' +
            '<span>' + result.pending + '%</span>' +
          '</div>' +
          '<div class="clearfix"></div>' +
        '</div>';

        widget__summary_html = approved_html + denied_html + canceled_html + pending_html;

        $('.summary-dashboard .widget__summary').html(widget__summary_html);
      },
      error: function() {

      }
    })
    
  });
  
  // validation email when create new user
  $('#UserNew #register #user_email').on('change', function(){
    var email = $(this).val();
    var check_data = {
      email: email
    }
    var email_html = '';
    $('.email_html').html('');
    $.ajax({
      url: "/check_user_email",
      type: "post",
      data: check_data,
      success: function(result) {
        if(result.status == "new") {
          email_html = 
          '<div class="approved_email">' +
            'This email can be used.' +
          '</div>';

        } else {
          email_html = 
          '<div class="rejected_email">' +
            'This email is already in use. Please use another email.' +
          '</div>';
        }
        $('.email_html').html(email_html);
      },
      error: function() {

      }
    })
  });

  // validation username when create new user
  $('#UserNew #register #user_username').on('change', function(){
    var username = $(this).val();
    var check_data = {
      username: username
    }
    var username_html = '';
    $('.username_html').html('');
    $.ajax({
      url: "/check_user_name",
      type: "post",
      data: check_data,
      success: function(result) {
        if(result.status == "new") {
          username_html = 
          '<div class="approved_username">' +
            'This username can be used.' +
          '</div>';

        } else {
          username_html = 
          '<div class="rejected_username">' +
            'This username is already in use. Please use another username.' +
          '</div>';
        }
        $('.username_html').html(username_html);
      },
      error: function() {

      }
    })
  });

});

$(function() {
  var start_dd = gon.event_start;
  var end_dd = gon.event_end;
  var start_d = '';
  var end_d = '';
  var user_role = gon.current_user_role;

  // date range picker
  var validStart_1 = '';
  var validEnd_1 = '';

  var d_start_1 = new Date();
  var d_start_1_date = d_start_1.getDate();
  var d_start_1_Month = d_start_1.getMonth();
  var d_start_1_week = d_start_1.getDay();

  var d_end_1 = new Date();
  var d_end_1_date = d_end_1.getDate();
  var d_end_1_Month = d_end_1.getMonth();
  var d_end_1_week = d_end_1.getDay();

  var dd_end_1 = new Date();
  var employee_end = '';

  if (user_role == 'admin') {
    // date rangepicker
    validStart_1 = false;
    validEnd_1 = false;
  } else if (user_role == 'manager') {
    validStart_1 = new Date();
    // date rangepicker
    d_end_1.setDate(d_end_1_date);
    d_end_1.setHours(23);
    d_end_1.setMinutes(59);
    d_end_1.setMonth(d_end_1_Month + 3);

    validEnd_1 = d_end_1;
  } else {
    // date rangepicker
    d_start_1.setDate(d_start_1_date + 3);
    d_start_1.setHours(2);
    d_start_1.setMinutes(0);
    
    //
    if (d_start_1.getDay() == 6) {
      d_add_date = 2;
    } else if (d_start_1.getDay() == 0) {
      d_add_date = 1;
    } else {
      d_add_date = 0;
    }
    d_start_1.setDate(d_start_1.getDate() + d_add_date);
    validStart_1 = d_start_1;

    dd_end_1.setDate(d_start_1.getDate());
    dd_end_1.setHours(23);
    dd_end_1.setMinutes(59);
    employee_end = dd_end_1;

    // date rangepicker
    d_end_1.setDate(d_end_1_date);
    d_end_1.setHours(23);
    d_end_1.setMinutes(59);
    d_end_1.setMonth(d_end_1_Month + 3);

    validEnd_1 = d_end_1;
  }
  var end_date = '';
  var startDay = '';
  var time_zone = Cookies.get('time_zone');
  if (start_dd && end_dd) {
    start_d = moment(new Date(start_dd));
    end_d = moment(new Date(end_dd));
  }
  else {
    if (user_role != 'employee') {
      start_d = moment().startOf('hour');
      end_d = moment().startOf('hour').add(32, 'hour');
    }
    else {
      start_d = new Date(validStart_1);
      start_d = moment(moment.tz(moment(validStart_1).format("LLL"), 'MMMM DD, YYYY hh:mm A', time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ"));
      end_d = moment(moment.tz(moment(employee_end).format("LLL"), 'MMMM DD, YYYY hh:mm A', time_zone).tz("utc").format("YYYY-MM-DDTHH:mm:ss.SSSSZ"));
      // startDay = new Date(start_d.getFullYear(), start_d.getMonth(), start_d.getDate(), start_d.getHours() - 1, 59, 59);
      // end_date = moment.tz(startDay, 'MMMM DD, YYYY hh:mm A', gon.time_zone).tz("utc")
    }
  }

  $('input[name="datetimes"]').daterangepicker({
    timePicker: true,
    startDate: start_d,
    endDate: end_d,
    minDate: validStart_1,
    maxDate: validEnd_1,
    locale: {
      format: 'MMMM DD, YYYY hh:mm A'
    }
  });


  var str_start = '';
  var str_end = '';

  if (gon.str_start && gon.str_end) {
    str_start = moment(new Date(gon.str_start));
    str_end = moment(new Date(gon.str_end));
    $('input[name="daterange"]').daterangepicker({
      // autoUpdateInput: false,
      startDate: str_start,
      endDate: str_end,
      locale: {
        cancelLabel: 'Clear',
        format: 'MMM DD, YYYY'
      }
    });
    $('input[name="start_date"]').val(str_start.format('YYYY-MM-DD'));
    $('input[name="end_date"]').val(str_end.format('YYYY-MM-DD'));
  }
  else {
    $('input[name="daterange"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear',
        format: 'MMM DD, YYYY'
      }
    });
  }
  

  $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
    $('input[name="start_date"]').val(picker.startDate.format('YYYY-MM-DD'));
    $('input[name="end_date"]').val(picker.endDate.format('YYYY-MM-DD'));
    $(this).val(picker.startDate.format('MMM DD, YYYY') + ' - ' + picker.endDate.format('MMM DD, YYYY'));
  });

  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
    $('input[name="start_date"]').val('');
    $('input[name="end_date"]').val('');
      $(this).val('');
  });

});

// disable inspect--------------
// document.onkeydown = function(e) {
//   if(event.keyCode == 123) {
//   return false;
//   }
//   if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
//   return false;
//   }
//   if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
//   return false;
//   }
//   if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
//   return false;
//   }
// }
//-------------end 'disable inspect'--------

$(document).ready(function() {
  $('#user_role_id').change(function(){
    
    var getRoleID = this.value;
    var data = {
      getRoleID: getRoleID
    }
    var select_html = '';
    var option_html = '';
    if (getRoleID == 3) {
      select_html = '<input type="hidden" name="user[manager_id]" id="user_manager_id" value="0">'
      $('#user_manager_id').closest('.field').html(select_html);
      select_html = '';
      select_html = '<input type="hidden" name="user[department_ids][]" id="user_department_ids" value="">'
      $('#user_department_ids').closest('.field').html(select_html);
    }
    else {
      $.ajax({
        type: "post",
        url: "/change_role",
        data: data,
        success: function(respond) {
          for (var index = 0; index < respond.managers.length; index++) {
            option_html += '<option value="' + respond.managers[index].id + '">' + respond.managers[index].full_name + '</option>';
          }
          select_html = 
          '<div class="field_wrapper">' +
            '<label for="user_manager_id">Manager *</label>' +
            '<select required="required" name="user[manager_id]" id="user_manager_id">' +
              option_html +
            '</select>' +
          '</div>';
          $('#user_manager_id').closest('.field').html(select_html);
          
          option_html = '';
          select_html = '';
          $('#department_multiples').attr("id", "user_department_ids");
          for(var index = 0; index < respond.departments.length; index ++) {
            var d = respond.departments[index];
            var leaf_html = '';
            if (d.has_child) {
              leaf_html = ' non-leaf';
            }
            if (d.is_root) {
              option_html += '<option value="' + d.id + '" class="l1 ' + leaf_html + '">' + d.name + '</option>';
            } else {
              option_html += '<option value="' + d.id + '" data-pup="' + d.parent_id + '" class="l' + d.check_level + leaf_html + '">' + d.name + '</option>';
            }
          }

          select_html = 
          '<div class="field_wrapper">' +
            '<label for="user_department_ids">Department *</label>' +
            '<select required="required" class="department_multiples" name="user[department_ids][]" id="user_department_ids" multiple="multiple">' +
              option_html +
            '</select>' +
          '</div>';
          $('#user_department_ids').closest('.field').html(select_html);

          //initialize when role is manager - for select2
          $('#user_department_ids').select2ToTree({
            width: 'calc(100% - 200px)'
          });
       

        },
        error: function() {

        }
       
      });
    }

  });

  $('#creator_id, #updater_id').change(function(e) {
    var getCreatorID = this.value;
    var select_html = '';
    var option_html = '';
    
    if(getCreatorID) {
      option_html = 
      '<option value="0">Use your timezone</option><option value="1">Use employee timezone</option>';
    }
    else {
      option_html = 
      '<option value="0">Use your timezone</option>';
    }
    if (e.target.id == 'creator_id') {
      $('#creator_tz').html(option_html);
    }
    else if (e.target.id == 'updater_id') {
      $('#updater_tz').html(option_html);
    }
  });
    
  $('.shift-dashboard').on('change', 'select.change_week', function() {
    var week_id = $(this).val();
    $(this).attr('name', 'shift[timeframes_attributes][][week_name]');
    $(this).closest('.field').find('.working_from').attr('name', 'shift[timeframes_attributes][][working_from]');
    $(this).closest('.field').find('.working_to').attr('name', 'shift[timeframes_attributes][][working_to]');
  });

  $('.shift-dashboard').on('click', '.btn-action .add_next_week', function() {
    var field_count = $('.shift-wrapper .field').length;
    if (field_count > 6) {
      return false;
    }
    var week_wrapper = '';
    week_wrapper = 
    '<div class="field-wrapper">' +
      '<select class="form-control change_week" required="required" name="shift[timeframes_attributes][][week_name]">' +
        '<option>Select</option>' +
        '<option value="Sunday">Sunday</option>' +
        '<option value="Monday">Monday</option>' +
        '<option value="Tuesday">Tuesday</option>' +
        '<option value="Wednesday">Wednesday</option>' +
        '<option value="Thursday">Thursday</option>' +
        '<option value="Friday">Friday</option>' +
        '<option value="Saturday">Saturday</option>' +
      '</select>' +
    '</div>';

    var time_wrapper = '';
    time_wrapper =
    '<div class="field-wrapper">' +
      '<input type="time" class="form-control custom_width working_from" required>' +
    '</div>' +
    '<div class="field-wrapper p_r-0">' +
      '<input type="time" class="form-control working_to" required>' +
    '</div>';
    var wrapper = 
    '<div class="field display-flex">' +
      week_wrapper +
      time_wrapper +
    '</div>'

    $('.shift-wrapper').append(wrapper);

  });

  $('.shift-dashboard').on('click', '.btn-action .delete_last_week', function() {
    var field_count = $('.shift-wrapper .field').length;
    if (field_count < 2) {
      return false;
    }
    $('.shift-wrapper .field:last').remove();
  });
  $('.all-shifts').click(function() {
    $('.view-all').delay(100).slideToggle("slideDown");
    $(this).text(function(i, text){
      return text === "Show all" ? "Hide all" : "Show all";
    })
    return false;
  });

  $('.penalty-dashboard').on('click', '.btn-confirm-penalty', function() {
    var user_id = $(this).data('user-id');
    var consequence_id = $(this).data('consequence-id');
    data = {
      id: user_id,
      consequence_id: consequence_id
    }
    $.ajax({
      type: "post",
      url: "/penalties/confirmed",
      data: data,
      success: function(result) {
        console.log("ok");
        var wrapper_html = '';
        wrapper_html = 
          '<svg class="checkmark checkmark_' + result.user_id + '" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>';
        $('[data-user-id="' + result.user_id + '"]').closest('td').html(wrapper_html);

        // var hide_element = '.checkmark_' + result.user_id;
        $('.checkmark_' + result.user_id).closest('tr').delay(4000).fadeOut();

        var consequence_html ='';
        consequence_html = 
          '<div class="flag-consequence display-flex">' +
            '<div class="flag-color-svg">' +
              '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" style="fill:' + result.flag_color + '" viewBox="0 0 24 24"><path style="stroke: #000000; stroke-width: 0.3;" d="M4 24h-2v-24h2v24zm18-22h-16v12h16l-4-5.969 4-6.031z"/></svg>' +
            '</div>' +
            '<p>' + result.consequence_full_info + '</p>' +
          '</div>'
        $('td[data-id="' + result.user_id + '"].consequence-step').html(consequence_html);

      },
      error: function() {

      }
    })
  })
  $('#penaltyNotificationTable').closest('.x_content').effect("slide", {}, 500);
  $('#penaltyNotificationTable').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_shift_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_shift_form').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_lunchtime_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_lunchtime_form').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_point_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_point_form').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_penalty_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_penalty_form').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_consequence_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_consequence_form').closest('.x_content').effect("highlight", {}, 1000);

  $('#edit_category_form').closest('.x_content').effect("slide", {}, 500);
  // $('#edit_category_form').closest('.x_content').effect("highlight", {}, 1000);


  $("#about-shift").click(function() {
    $('html, body').animate({
        scrollTop: $("#about-shift").offset().top
    }, 2000);
    
  });


  // ------------ Help nav menu-----------

  // $('.help_menu_side .nav a').on('click', function(event) {
  //   $('#sidebar-menu').find('a').removeClass('active');
  //   $(this).addClass('active');
  // });

  // $(window).on('scroll', function() {
  //     $('.about-section').each(function() {
  //         if($(window).scrollTop() >= $(this).offset().top) {
  //             var id = $(this).attr('id');
  //             $('#sidebar-menu a').removeClass('active');
  //             $('.help_menu_side .nav a[href="#'+ id +'"]').addClass('active');
  //         }
  //     });
  // });

  $('input[name="holidaytype[duration]"]').on('change', function() {
    var type = $(this).val();
    var holiday__time = '';
    if (type == 'full_day') {
      holiday__time = 
      '<div class="field display-flex">' +
        '<div class="field-wrapper">' +
          '<label for="start_time">From *</label><br>' +
          '<input class="form-control" required="required" value="00:00" readonly="readonly" type="time" name="holidaytype[start_time]" id="holidaytype_start_time">' +
        '</div>' +
        '<div class="field-wrapper">' +
          '<label for="end_time">To *</label><br>' +
          '<input class="form-control" required="required" value="23:59" readonly="readonly" type="time" name="holidaytype[end_time]" id="holidaytype_end_time">' +
        '</div>' +
      '</div>'
    } else if (type == 'half_forenoon') {
      holiday__time = 
      '<div class="field display-flex">' +
        '<div class="field-wrapper">' +
          '<label for="start_time">From *</label><br>' +
          '<input class="form-control" required="required" value="08:00" readonly="readonly" type="time" name="holidaytype[start_time]" id="holidaytype_start_time">' +
        '</div>' +
        '<div class="field-wrapper">' +
          '<label for="end_time">To *</label><br>' +
          '<input class="form-control" required="required" value="12:00" readonly="readonly" type="time" name="holidaytype[end_time]" id="holidaytype_end_time">' +
        '</div>' +
      '</div>'
    } else if (type == 'half_afternoon') {
      holiday__time = 
      '<div class="field display-flex">' +
        '<div class="field-wrapper">' +
          '<label for="start_time">From *</label><br>' +
          '<input class="form-control" required="required" value="14:00" readonly="readonly" type="time" name="holidaytype[start_time]" id="holidaytype_start_time">' +
        '</div>' +
        '<div class="field-wrapper">' +
          '<label for="end_time">To *</label><br>' +
          '<input class="form-control" required="required" value="18:00" readonly="readonly" type="time" name="holidaytype[end_time]" id="holidaytype_end_time">' +
        '</div>' +
      '</div>'
    } else {
      holiday__time = 
      '<div class="field display-flex">' +
        '<div class="field-wrapper">' +
          '<label for="start_time">From *</label><br>' +
          '<input class="form-control" required="required" value="08:00" type="time" name="holidaytype[start_time]" id="holidaytype_start_time">' +
        '</div>' +
        '<div class="field-wrapper">' +
          '<label for="end_time">To *</label><br>' +
          '<input class="form-control" required="required" value="18:00" type="time" name="holidaytype[end_time]" id="holidaytype_end_time">' +
        '</div>' +
      '</div>'
    }

    $('.holiday_type-dashboard .holiday__time').html(holiday__time);
  });


  $('input[type=password]').keyup(function() {

    var currentID = $(this).attr('id');
    // set password variable
    
    var pswd_compare = $('#user_password_confirmation').val();
    //validate the length
    if(currentID == 'user_password') {
      var pswd = $(this).val();
      if ( pswd.length < 8 ) {
        $('#length').removeClass('valid').addClass('invalid');
      } else {
        $('#length').removeClass('invalid').addClass('valid');
      }

      //validate letter
      if ( pswd.match(/[A-z]/) ) {
        $('#letter').removeClass('invalid').addClass('valid');
      } else {
        $('#letter').removeClass('valid').addClass('invalid');
      }

      //validate capital letter
      if ( pswd.match(/[A-Z]/) ) {
        $('#capital').removeClass('invalid').addClass('valid');
      } else {
        $('#capital').removeClass('valid').addClass('invalid');
      }

      //validate number
      if ( pswd.match(/\d/) ) {
        $('#number').removeClass('invalid').addClass('valid');
      } else {
        $('#number').removeClass('valid').addClass('invalid');
      }
    } else {
      var pswd = $('#user_password').val();
      var pswd_compare = $(this).val();

      if(pswd == pswd_compare) {
        $('#equal').removeClass('invalid').addClass('valid');
      } else {
        $('#equal').removeClass('valid').addClass('invalid');
      }
    }

    //password confirm
    
  }).focus(function() {
      $('#pswd_info').show();
  }).blur(function() {
      $('#pswd_info').hide();
  });

  $('.roles-dashboard').on('change', '.check_full', function() {
    if($(this).is(":checked")) {
      $('.check_field').prop('checked', true);
    } else {
      $('.check_field').prop('checked', false);
    }
  });
  $('.roles-dashboard').on('change', '.check_field', function() {
    var check_field_length = $('.check_field').length;
    var checked_field_length = $('.check_field:checked').length;
    if (checked_field_length == check_field_length) {
      $('.check_full').prop('checked', true);
    } else {
      $('.check_full').prop('checked', false);
    }

  })

  
})


