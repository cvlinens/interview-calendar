# == Schema Information
#
# Table name: points
#
#  id            :bigint           not null, primary key
#  description   :text
#  disabled      :boolean          default(FALSE)
#  name          :string
#  penalty_point :decimal(5, 3)
#  slug          :string
#  updated_user  :bigint
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  original_id   :bigint
#
# Indexes
#
#  index_points_on_name  (name) UNIQUE
#  index_points_on_slug  (slug) UNIQUE
#

class Point < ApplicationRecord
  
  has_many :penalties, dependent: :destroy
  extend FriendlyId
  friendly_id :name, use: :slugged

  after_update :get_users
  
  def get_users
    self.penalties.each do |p|
      if p.user.find_consequence_id < p.user.consequence_id
        p.user.confirm_penalty(p.user.find_consequence_id)
      end
    end
  end

  def history
    temp = self
    index = 0
    temp_points = []
    until temp.original_id == nil 
      index = index + 1
      temp = Point.find temp.original_id
      temp_points << temp.id
    end
    temp_points
  end
end
