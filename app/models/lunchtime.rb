# == Schema Information
#
# Table name: lunchtimes
#
#  id             :bigint           not null, primary key
#  lunchtime_from :string
#  lunchtime_to   :string
#  name           :string
#  slug           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_lunchtimes_on_name  (name) UNIQUE
#  index_lunchtimes_on_slug  (slug) UNIQUE
#

class Lunchtime < ApplicationRecord
  has_many :user
  validates_uniqueness_of :name
  extend FriendlyId
  friendly_id :name, use: :slugged

  def full_info
    return "#{name} (#{Dates.time_12(lunchtime_from, lunchtime_to)})"
  end
end
