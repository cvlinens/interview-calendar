# == Schema Information
#
# Table name: roles
#
#  id             :bigint           not null, primary key
#  category       :boolean          default(FALSE)
#  consequence    :boolean          default(FALSE)
#  department     :boolean          default(FALSE)
#  event_creation :boolean          default(TRUE)
#  event_status   :boolean          default(FALSE)
#  full_access    :boolean          default(FALSE)
#  holiday        :boolean          default(FALSE)
#  lunchtime      :boolean          default(FALSE)
#  name           :string
#  penalty        :boolean          default(FALSE)
#  point          :boolean          default(FALSE)
#  shift          :boolean          default(FALSE)
#  user           :boolean          default(FALSE)
#  user_role      :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Role < ApplicationRecord
  has_many :users, dependent: :destroy


end
