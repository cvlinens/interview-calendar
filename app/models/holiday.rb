# == Schema Information
#
# Table name: holidays
#
#  id             :bigint           not null, primary key
#  holiday_date   :datetime
#  name           :string
#  slug           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  holidaytype_id :integer
#
# Indexes
#
#  index_holidays_on_slug  (slug) UNIQUE
#

class Holiday < ApplicationRecord
  belongs_to :holidaytype
  validates_uniqueness_of :name
  extend FriendlyId
  friendly_id :name, use: :slugged

  
end
