# == Schema Information
#
# Table name: events
#
#  id          :bigint           not null, primary key
#  comment     :text
#  description :string
#  end_date    :datetime
#  start_date  :datetime
#  status      :integer          default(0)
#  status_by   :bigint           default(0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  category_id :bigint           default(1)
#  user_id     :bigint
#

class Event < ApplicationRecord
  belongs_to :user
  belongs_to :category

  attr_accessor :current_user
  
  validate :check_limit
  
  # after_destroy :send_delete_mail

  scope :order_created_asc, -> { order(created_at: :asc) }
  scope :order_created_desc, -> { order(created_at: :desc) }

  def self.strong_parameters
    columns =[:description, :start_date, :end_date, :time_zone, :category_id, :user_id, :comment]
  end

  # def send_delete_mail
  #   UserMailer.delete_event_notification(self, current_user).deliver_later
  # end

  def category_name
    if category.present?
      category.name
    else
      "NO CATEGORY"
    end
  end
  
  def approved?
    status == 3
  end

  def denied?
    status == 2
  end

  def canceled?
    status == 1
  end

  def pending?
    status == 0
  end

  def changed_by
    user =  User.find(self.status_by)
    full_name = "#{user.first_name} #{user.last_name}"
    role_name = Role.find(user.role_id).name.capitalize!
    changed_by = "#{full_name}(#{role_name})"
    return changed_by
  end

  def created_by
    user = User.find(self.user_id)
    full_name = "#{user.first_name} #{user.last_name}"
    role_name = Role.find(user.role_id).name.capitalize!
    created_by = "#{full_name}(#{role_name})"
    return created_by
  end

  def check_status
    if self.approved?
      event_status = "approved"
    elsif self.denied?
      event_status = "denied"
    elsif self.canceled?
      event_status = "canceled"
    else
      event_status = "pending"
    end
    return event_status
  end

  def start_d
    self.start_date.strftime("%B %e, %Y")
  end

  def start_t
    self.start_date.strftime("%l:%M %p")
  end

  def end_d
    self.end_date.strftime("%B %e, %Y")  
  end

  def end_t
    self.end_date.strftime("%l:%M %p")  
  end

  def start_D
    "#{self.start_date.strftime("%B %e, %Y")  } #{self.start_date.strftime("%l:%M %p")}"
  end

  def end_D
    "#{self.end_date.strftime("%B %e, %Y")  } #{self.end_date.strftime("%l:%M %p")}"
  end

  def created_D
    "#{self.created_at.strftime("%b %d,%l:%M %p")}"
  end

  def updated_D
    "#{self.updated_at.strftime("%b %d,%l:%M %p")}"
  end


  def start_week
    case self.start_date.wday
    when 0
      "Sunday"
    when 1
      "Monday"
    when 2
      "Tuesday"
    when 3
      "Wednesday"
    when 4
      "Thursday"
    when 5
      "Friday"
    when 6
      "Saturday"
    else
      "Error: invalid week"
    end
  end

  def end_week
    case self.end_date.wday
    when 0
      "Sunday"
    when 1
      "Monday"
    when 2
      "Tuesday"
    when 3
      "Wednesday"
    when 4
      "Thursday"
    when 5
      "Friday"
    when 6
      "Saturday"
    else
      "Error: invalid week"
    end
  end

  def event_off_time
    start_date = self.start_date
    end_date = self.end_date
    count_number = (end_date - start_date).to_i / 86400
    
    timeframes = self.user.shift.timeframes
    start = start_date.in_time_zone(self.user.time_zone)
    finish = end_date.in_time_zone(self.user.time_zone)

    off_time = 0
    i = 0
    while(start < finish) do
      yy = start.year
      mm = start.month
      dd = start.day
      t_f = timeframes.find_by(week_name: start.strftime("%A"))
      
      # get shift time for every event
      if t_f.blank?
        start += 1.day
        i += 1
        next
      end
      start_shift = t_f.working_from.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      end_shift = t_f.working_to.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      # get lunchtime
      start_lunchtime = self.user.lunchtime.lunchtime_from.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      end_lunchtime = self.user.lunchtime.lunchtime_to.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      
      count_number2 = (finish - start).to_f / 3600
      if count_number == 0
        if start != finish
          if start >= end_lunchtime
            lunchtime = 0
          elsif start <= start_lunchtime
            if finish >= end_lunchtime
              lunchtime = self.user.get_off_time_from_lunchtime
            elsif finish <= start_lunchtime
              lunchtime = 0
            else
              lunchtime = (finish - start_lunchtime).to_f / 3600 
            end
          else
            if finish >= end_lunchtime
              lunchtime = (end_lunchtime - start).to_f / 3600
            else
              lunchtime = (finish - start).to_f / 3600
            end
          end

          if start >= end_shift
            shift_time = 0
          elsif start <= start_shift
            if finish >= end_shift
              shift_time = (end_shift - start_shift).to_f / 3600
            elsif finish <= start_shift
              shift_time = 0
            else
              shift_time = (finish - start_shift).to_f / 3600
            end
          else
            if finish >= end_shift
              shift_time = (end_shift - start).to_f / 3600
            else
              shift_time = (finish - start).to_f / 3600
            end
          end
            
        else
          shift_time = 0
          lunchtime = 0
        end
        holiday_hours = 0
        Holiday.all.each do |holiday|
          holiday_start = holiday.holiday_date.beginning_of_day.in_time_zone(self.user.time_zone)
          holiday_end = holiday.holiday_date.end_of_day.in_time_zone(self.user.time_zone)
          s_holiday = holiday.holidaytype.start_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          e_holiday = holiday.holidaytype.end_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          if start >= holiday_start && start <= holiday_end
            if holiday.holidaytype.duration == 'full_day'
              holiday_hours = holiday.holidaytype.hours_off
              shift_time = holiday.holidaytype.hours_off
              lunchtime = 0
            else holiday.holidaytype.duration == 'half_forenoon' || holiday.holidaytype.duration == 'half_afternoon'
              if start <= s_holiday
                holiday_hours = holiday.holidaytype.hours_off
              elsif start >= e_holiday 
                holiday_hours = 0
              else
                holiday_hours = (e_holiday - start).to_f / 3600
              end
            end
          else
            next
          end
        end
        off_time = off_time + shift_time - lunchtime - holiday_hours

        if count_number2 > 15
          start = finish
          start = start.change(hour: 00, minute: 00, second: 00)
          next
        else
          start += 1.day
          next
        end
      else
        if i == 0
        elsif i > 0 && i < count_number
          start = start.change(hour: 00, minute: 00, second: 00)
        else
          start = finish
        end
        if start != finish
          if start <= start_lunchtime
            lunchtime = self.user.get_off_time_from_lunchtime
          elsif start > start_lunchtime && start <= end_lunchtime
            lunchtime = (end_lunchtime - start).to_f / 3600
          else
            lunchtime = 0
          end 
          if start_shift >= start
            shift_time = (end_shift - start_shift).to_f / 3600
          elsif start_shift < start && end_shift >= start
            shift_time = (end_shift - start).to_f / 3600
          else
            shift_time = 0
          end
        else
          if finish >= end_lunchtime
            lunchtime = self.user.get_off_time_from_lunchtime
          elsif finish >= start_lunchtime && finish < end_lunchtime
            lunchtime = (finish - start_lunchtime).to_f / 3600
          else
            lunchtime = 0
          end 
          if start_shift >= finish
            shift_time = 0
          elsif start_shift < finish && end_shift >= finish
            shift_time = (finish - start_shift).to_f / 3600
          else
            shift_time = (end_shift - start_shift).to_f / 3600
          end
        end
        holiday_hours = 0
        Holiday.all.each do |holiday|
          holiday_start = holiday.holiday_date.beginning_of_day.in_time_zone(self.user.time_zone)
          holiday_end = holiday.holiday_date.end_of_day.in_time_zone(self.user.time_zone)
          s_holiday = holiday.holidaytype.start_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          e_holiday = holiday.holidaytype.end_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          if start >= holiday_start && start <= holiday_end
            if holiday.holidaytype.duration == 'full_day'
              holiday_hours = holiday.holidaytype.hours_off
              shift_time = holiday.holidaytype.hours_off
              lunchtime = 0
            else 
              if start <= s_holiday
                holiday_hours = holiday.holidaytype.hours_off
              elsif start >= e_holiday 
                holiday_hours = 0
              else
                holiday_hours = (e_holiday - start).to_f / 3600
              end
            end
          else
            next
          end
        end
        off_time = off_time + shift_time - lunchtime - holiday_hours
      end
    
      start += 1.day
      i += 1
    end
    off_time
  end

  def custom_off_time(start_date, end_date)
    count_number = (end_date - start_date).to_i / 86400
    
    timeframes = self.user.shift.timeframes
    start = start_date.in_time_zone(self.user.time_zone)
    finish = end_date.in_time_zone(self.user.time_zone)

    off_time = 0
    i = 0
    while(start < finish) do
      yy = start.year
      mm = start.month
      dd = start.day
      t_f = timeframes.find_by(week_name: start.strftime("%A"))
      
      # get shift time for every event
      if t_f.blank?
        start += 1.day
        i += 1
        next
      end
      start_shift = t_f.working_from.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      end_shift = t_f.working_to.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      # get lunchtime
      start_lunchtime = self.user.lunchtime.lunchtime_from.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      end_lunchtime = self.user.lunchtime.lunchtime_to.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
      
      
      count_number2 = (finish - start).to_f / 3600
      if count_number == 0
        if start != finish
          if start >= end_lunchtime
            lunchtime = 0
          elsif start <= start_lunchtime
            if finish >= end_lunchtime
              lunchtime = self.user.get_off_time_from_lunchtime
            elsif finish <= start_lunchtime
              lunchtime = 0
            else
              lunchtime = (finish - start_lunchtime).to_f / 3600 
            end
          else
            if finish >= end_lunchtime
              lunchtime = (end_lunchtime - start).to_f / 3600
            else
              lunchtime = (finish - start).to_f / 3600
            end
          end

          if start >= end_shift
            shift_time = 0
          elsif start <= start_shift
            if finish >= end_shift
              shift_time = (end_shift - start_shift).to_f / 3600
            elsif finish <= start_shift
              shift_time = 0
            else
              shift_time = (finish - start_shift).to_f / 3600
            end
          else
            if finish >= end_shift
              shift_time = (end_shift - start).to_f / 3600
            else
              shift_time = (finish - start).to_f / 3600
            end
          end
            
        else
          shift_time = 0
          lunchtime = 0
        end
        holiday_hours = 0
        Holiday.all.each do |holiday|
          holiday_start = holiday.holiday_date.beginning_of_day.in_time_zone(self.user.time_zone)
          holiday_end = holiday.holiday_date.end_of_day.in_time_zone(self.user.time_zone)
          s_holiday = holiday.holidaytype.start_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          e_holiday = holiday.holidaytype.end_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          if start >= holiday_start && start <= holiday_end
            if holiday.holidaytype.duration == 'full_day'
              holiday_hours = holiday.holidaytype.hours_off
              shift_time = holiday.holidaytype.hours_off
              lunchtime = 0
            else holiday.holidaytype.duration == 'half_forenoon' || holiday.holidaytype.duration == 'half_afternoon'
              if start <= s_holiday
                holiday_hours = holiday.holidaytype.hours_off
              elsif start >= e_holiday 
                holiday_hours = 0
              else
                holiday_hours = (e_holiday - start).to_f / 3600
              end
            end
          else
            next
          end
        end
        off_time = off_time + shift_time - lunchtime - holiday_hours

        if count_number2 > 15
          start = finish
          start = start.change(hour: 00, minute: 00, second: 00)
          next
        else
          start += 1.day
          next
        end
      else
        if i == 0
        elsif i > 0 && i < count_number
          start = start.change(hour: 00, minute: 00, second: 00)
        else
          start = finish
        end
        if start != finish
          if start <= start_lunchtime
            lunchtime = self.user.get_off_time_from_lunchtime
          elsif start > start_lunchtime && start <= end_lunchtime
            lunchtime = (end_lunchtime - start).to_f / 3600
          else
            lunchtime = 0
          end 
          if start_shift >= start
            shift_time = (end_shift - start_shift).to_f / 3600
          elsif start_shift < start && end_shift >= start
            shift_time = (end_shift - start).to_f / 3600
          else
            shift_time = 0
          end
        else
          if finish >= end_lunchtime
            lunchtime = self.user.get_off_time_from_lunchtime
          elsif finish >= start_lunchtime && finish < end_lunchtime
            lunchtime = (finish - start_lunchtime).to_f / 3600
          else
            lunchtime = 0
          end 
          if start_shift >= finish
            shift_time = 0
          elsif start_shift < finish && end_shift >= finish
            shift_time = (finish - start_shift).to_f / 3600
          else
            shift_time = (end_shift - start_shift).to_f / 3600
          end
        end
        holiday_hours = 0
        Holiday.all.each do |holiday|
          holiday_start = holiday.holiday_date.beginning_of_day.in_time_zone(self.user.time_zone)
          holiday_end = holiday.holiday_date.end_of_day.in_time_zone(self.user.time_zone)
          s_holiday = holiday.holidaytype.start_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          e_holiday = holiday.holidaytype.end_time.to_time.asctime.in_time_zone(self.user.time_zone).change(year: yy, month: mm, day: dd)
          if start >= holiday_start && start <= holiday_end
            if holiday.holidaytype.duration == 'full_day'
              holiday_hours = holiday.holidaytype.hours_off
              shift_time = holiday.holidaytype.hours_off
              lunchtime = 0
            else holiday.holidaytype.duration == 'half_forenoon' || holiday.holidaytype.duration == 'half_afternoon'
              if start <= s_holiday
                holiday_hours = holiday.holidaytype.hours_off
              elsif start >= e_holiday 
                holiday_hours = 0
              else
                holiday_hours = (e_holiday - start).to_f / 3600
              end
            end
          else
            next
          end
        end
        off_time = off_time + shift_time - lunchtime - holiday_hours
      end
    
      start += 1.day
      i += 1
    end
    off_time
  end

  def include_middle_duration?
    start_date = self.start_date
    end_date = self.end_date
    s = 'March 1'
    f = 'September 30'
    start = s.to_datetime
    finish = f.to_datetime
    start = start.asctime.in_time_zone(self.user.time_zone)
    finish = finish.asctime.in_time_zone(self.user.time_zone).end_of_day
    start_UTC = start.utc
    end_UTC = finish.utc

    if start_date > end_UTC
      return false
    elsif end_date < start_UTC
      return false
    else
      return true
    end
  end

  def middle_off_time_single
    s = 'March 1'
    f = 'September 30'
    start = s.to_datetime
    finish = f.to_datetime
    start = start.asctime.in_time_zone(self.user.time_zone)
    finish = finish.asctime.in_time_zone(self.user.time_zone).end_of_day
    start_UTC = start.utc
    end_UTC = finish.utc

    start_event = self.start_date
    end_event = self.end_date
    if start_event > end_UTC
      off_time = 0
    elsif start_event < start_UTC
      if end_event > end_UTC
        off_time = self.custom_off_time(start_UTC, end_UTC)
      elsif end_event < start_UTC
        off_time = 0
      else
        off_time = self.custom_off_time(start_UTC, end_event)
      end
    else
      if end_event >= end_UTC
        off_time = self.custom_off_time(start_event, end_UTC)
      else
        off_time = self.custom_off_time(start_event, end_event)
      end
    end
    off_time
  end

  private

    def check_limit
      
      if self.user.total_off_time_user >= 176
        errors.add(:title, "Total Off Time has already reached to 176 hours.")
      else
        if new_record?
          if self.user.total_off_time_user + self.event_off_time > 176
            exceed_time = self.user.total_off_time_user + self.event_off_time - 176
            errors.add(:title, "Failed. This event exceed #{exceed_time} hours.")
          else
            if self.include_middle_duration?
              if self.middle_off_time_single >= 96
                errors.add(:title, "Failed. Between March 1 and September 30, the off time has already reached to 96 hours.")
              else
                if self.user.middle_off_time + self.middle_off_time_single > 96
                  exceed_time = self.user.middle_off_time + self.middle_off_time_single - 96
                  errors.add(:title, "Failed. Between March 1 and September 30, this event exceed #{exceed_time} hours .")
                end
              end
            end
          end
        else
          origin_event = Event.find self.id
          if origin_event == 0 && self.status == 3
            if self.user.total_off_time_user + self.event_off_time > 176
              exceed_time = self.user.total_off_time_user + self.event_off_time - 176
              errors.add(:title, "The event approval is failed. This event exceed #{exceed_time} hours.")
            else
              if self.include_middle_duration?
                if self.middle_off_time_single >= 96
                  errors.add(:title, "Failed. Between March 1 and September 30, the off time has already reached to 96 hours.")
                else
                  if self.user.middle_off_time + self.middle_off_time_single > 96
                    exceed_time = self.user.middle_off_time + self.middle_off_time_single - 96
                    errors.add(:title, "Failed. Between March 1 and September 30, this event exceed #{exceed_time} hours .")
                  end
                end
              end
            end
          else
            total_off = self.user.total_off_time_user - origin_event.event_off_time
            if total_off + self.event_off_time > 176
              exceed_time = total_off + self.event_off_time - 176
              errors.add(:title, "The event update is failed. This event exceed #{exceed_time} hours.")
            end
          end
        end
      end

    end

end
