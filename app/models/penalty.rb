# == Schema Information
#
# Table name: penalties
#
#  id          :bigint           not null, primary key
#  apply_date  :datetime
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  point_id    :integer
#  user_id     :bigint
#

class Penalty < ApplicationRecord
  belongs_to :user
  belongs_to :point, optional: true

  after_destroy :update_consequence_id


  def update_consequence_id
    if self.user.find_consequence_id != self.user.consequence_id
      self.user.confirm_penalty(self.user.find_consequence_id)
    end
  end


  
end
