# == Schema Information
#
# Table name: consequences
#
#  id          :bigint           not null, primary key
#  description :string
#  flag_color  :string           default("#FFFFFF")
#  reach_point :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Consequence < ApplicationRecord
  has_many :users

  def full_info
    return "#{self.reach_point} - Points"
  end

  def next
    Consequence.where("id > ?", id).first
  end

  def prev
    Consequence.where("id < ?", id).last
  end
end
