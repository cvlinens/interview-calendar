# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  active                 :boolean          default(TRUE)
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  sign_in_count          :integer          default(0), not null
#  slug                   :string
#  time_zone              :string           default("UTC")
#  user_point_limit       :decimal(5, 3)
#  username               :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  consequence_id         :integer          default(0)
#  lunchtime_id           :bigint           default(1)
#  manager_id             :bigint
#  role_id                :bigint
#  shift_id               :bigint           default(1)
#
# Indexes
#
#  index_users_on_email     (email) UNIQUE
#  index_users_on_role_id   (role_id)
#  index_users_on_slug      (slug) UNIQUE
#  index_users_on_username  (username) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (role_id => roles.id)
#

class User < ApplicationRecord
  belongs_to :role
  belongs_to :shift, optional: true
  belongs_to :lunchtime, optional: true
  belongs_to :consequence, optional: true
  after_create :set_user_point
  has_many :penalties, dependent: :destroy
  has_and_belongs_to_many :departments

  has_many :events, dependent: :destroy
  # before_create :set_default_role
  # or 
  before_validation :set_default_role 
  # before_validation :get_ldap_email
  # before_validation :get_ldap_id
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :recoverable, :rememberable, :trackable, :validatable, :database_authenticatable

  attr_writer :login
  validates :username, presence: :true, uniqueness: { case_sensitive: false }
  validate :validate_username
  # after_destroy :send_destroy_mail
  attr_accessor :current_user      # current user in model
  
  extend FriendlyId
  friendly_id :username, use: :slugged

  # def self.strong_parameters
  #   columns =[:id, :first_name, :last_name, :username, :email, :role_id, :manager_id, :time_zone, :shift_id, :lunchtime_id, :password, :password_confirmation, department_ids: []]
  # end

  # def send_destroy_mail
  #   UserMailer.delete_user_notification(self, current_user).deliver_later
  # end


  ###_-------------------------------- LDAP ---------------------------------##############
  def ldap_before_save
    self.email = Devise::LDAP::Adapter.get_ldap_param(self.username,"mail").first
  end
  
  def get_ldap_email
    self.email = Devise::LDAP::Adapter.get_ldap_param(self.username,"mail").first
  end


  def get_ldap_id
    self.id = Devise::LDAP::Adapter.get_ldap_param(self.username,"uidnumber").first
  end

  def authenticatable_salt
    Digest::SHA1.hexdigest(email)[0,29]
  end

  ###_-------------------------------- LDAP ---------------------------------##############


  def set_user_point
    current_month = Time.now.month
    last_day_of_month = Time.now.end_of_month.day
    current_day = Time.now.day
    total_point = 15

    ratio = total_point / 12.to_f

    user_get_point = (12 - current_month + 1) * ratio

    if current_day >= last_day_of_month / 2.to_f
      user_get_point = user_get_point - ratio / 2.to_f
    end

    self.user_point_limit = user_get_point
    self.save!
  end

  def confirm_penalty(consequence_id)
    if consequence_id == 0
      update_column :consequence_id, 0
    else
      consequence = Consequence.find(consequence_id)
      update_column :consequence_id, consequence_id
    end
  end

  def update_consequence_id
    consequence = Consequence.find(self.consequence_id)
    if self.consequence_id == Consequence.first.id
      update_column :consequence_id, 0
    else
      update_column :consequence_id, consequence.prev.id
    end
  end

  def check_penalty?
    if self.consequence_id == 0
      consequence = Consequence.first
      consequence_point = consequence.reach_point
    else
      consequence = Consequence.find(self.consequence_id).next
      consequence_point = consequence.reach_point
    end
    
    self.total_penalty >= consequence_point
  end

  def total_penalty
    t = 0
    penalties.each do |p|
      t = t + p.point.penalty_point
    end
    t
  end

  def find_consequence_id
    consequence_id = 0
    Consequence.all.each do |cc|
      if cc != Consequence.last
        if self.total_penalty >= cc.reach_point && self.total_penalty < cc.next.reach_point
          consequence_id = cc.id
        end
      end
    end
    consequence_id
  end

  def get_holidays(start_date, end_date)
    holidays=[]
    Holiday.all.each do |holiday|
      if holiday.holiday_date >= start_date && holiday.holiday_date.end_of_day <= end_date
        holidays << holiday
      end
    end
    holidays
  end

  def get_hours_for_holidays(start_date, end_date)
    holidays=[]
    holiday_hours = 0
    Holiday.all.each do |holiday|
      if ((holiday.holiday_date >= start_date && holiday.holiday_date.end_of_day <= end_date) || (holiday.holiday_date <= start_date && holiday.holiday_date.end_of_day >= start_date) || (holiday.holiday_date >= end_date && holiday.holiday_date.end_of_day <= end_date))
        holiday_hours = holiday_hours + holiday.holidaytype.hours_off
      end
    end
    holiday_hours
  end

  def total_off_time_user
    own_events = Event.where(user_id: self.id)
    own_events = own_events.where(status: 3)
    total_off_time_user = 0
    own_events.each do |event|
      total_off_time_user = total_off_time_user + event.event_off_time
    end
    total_off_time_user
  end

  def total_off_time_group
    events = self.events_by_role
    total_off_time_group = 0
    events.each do |event|
      total_off_time_group = total_off_time_group + event.event_off_time
    end
    total_off_time_group
  end

  def get_remain_hours

  end

  def middle_off_time
    s = 'March 1'
    f = 'September 30'
    start = s.to_datetime
    finish = f.to_datetime
    start = start.asctime.in_time_zone(self.time_zone)
    finish = finish.asctime.in_time_zone(self.time_zone).end_of_day
    start_UTC = start.utc
    end_UTC = finish.utc
    
    own_events = Event.where(user_id:self.id).where(status: 3)
    own_events = own_events.where('start_date >= ?', start_UTC).where('start_date <= ?', end_UTC).or(own_events.where('end_date >= ?', start_UTC).where('end_date <= ?', end_UTC)).or(own_events.where('start_date < ?', start_UTC).where('end_date > ?', end_UTC))

    middle_off_time = 0
    own_events.each do |event|
      middle_off_time = middle_off_time + event.middle_off_time_single
    end
    middle_off_time
  end
  
  def get_off_time_from_lunchtime
    yy = Time.now.year
    mm = Time.now.month
    dd = Time.now.day
    start_lunchtime = self.lunchtime.lunchtime_from.to_time.asctime.in_time_zone(self.time_zone).change(year: yy, month: mm, day: dd)
    end_lunchtime = self.lunchtime.lunchtime_to.to_time.asctime.in_time_zone(self.time_zone).change(year: yy, month: mm, day: dd)
    lunch = (end_lunchtime - start_lunchtime).to_f / 3600
    return lunch
  end

  def to_param
    username
  end

  def login
    @login || self.username || self.email
  end

  def active?
    active == true
  end

  def inactive?
    active == false
  end

  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def full_name_role
    "#{self.first_name} #{self.last_name} (#{Role.find(self.role_id).name})"
  end
  
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end

  def admin?
    role_id == 3 || role.full_access == true
  end

  def manager?
    role_id == 2
  end

  def employee?
    role_id == 1
  end

  def full_perm?
    role.full_access == true
  end

  def event_create_perm?
    role.event_creation == true
  end

  def event_status_perm?
    role.event_status == true
  end

  def user_perm?
    role.user == true
  end

  def category_perm?
    role.category == true
  end

  def shift_perm?
    role.shift == true
  end

  def lunchtime_perm?
    role.lunchtime == true
  end

  def holiday_perm?
    role.holiday == true
  end

  def penalty_perm?
    role.penalty == true
  end

  def point_perm?
    role.point == true
  end

  def user_role_perm?
    role.user_role == true
  end

  def department_perm?
    role.department == true
  end

  def consequence_perm?
    role.consequence == true
  end

  def user_role
    return "#{Role.find(self.role_id).name}"
  end

  def find_own_manager
    return User.find(self.manager_id)
  end

  def events_by_role
    if admin?
      events = Event.all
    elsif manager?
      # has to be updated
      employee_ids = User.where(manager_id: id).pluck(:id)
      user_ids = employee_ids.push(id) # add self id(manager id)
      events = Event.where(user_id: user_ids)
    else
      events = Event.where(user_id: id)
    end
    events.order(created_at: :asc)
  end

  def employees
    User.where(manager_id: id, role_id: 1)
  end
  
  def check_date?
    count_number = 0
    self.events.each do |d|
      
      count_number = count_number + ((d.end_date - d.start_date) / 86400).to_i

    end
    count_number

  end

  def created
    return "#{self.created_at.strftime("%B %e, %Y")} #{self.created_at.strftime("%l:%M %P")}"
  end

  def find_my_employees
    employee_ids = User.where(manager_id: id).pluck(:id)
    user_ids = employee_ids.push(id) # add self id(manager id)
    return User.where(id: user_ids)
  end

  private
  def set_default_role
    self.role ||= Role.find_by_name('employee')
  end
end
