# == Schema Information
#
# Table name: holidaytypes
#
#  id         :bigint           not null, primary key
#  duration   :string
#  end_time   :string
#  hours_off  :integer
#  name       :string
#  slug       :string
#  start_time :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_holidaytypes_on_slug  (slug) UNIQUE
#

class Holidaytype < ApplicationRecord
  has_many :holidays, dependent: :destroy
  validates_uniqueness_of :name
  extend FriendlyId
  friendly_id :name, use: :slugged
  def full_info
    return "#{self.name} - #{self.hours_off}hours (#{self.start_time} - #{self.end_time})"
  end
  def time_info
    return "#{self.start_time} - #{self.end_time}"
  end

  def full_time_info
    return "#{self.name} - #{self.hours_off}hours (#{Dates.time_12(self.start_time, self.end_time)})"
  end
  
end
