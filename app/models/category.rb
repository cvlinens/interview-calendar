# == Schema Information
#
# Table name: categories
#
#  id             :bigint           not null, primary key
#  default_option :boolean          default(FALSE)
#  description    :text
#  name           :string
#  slug           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_categories_on_name  (name) UNIQUE
#  index_categories_on_slug  (slug) UNIQUE
#

class Category < ApplicationRecord
  has_many :event
  validates_uniqueness_of :name
  extend FriendlyId
  friendly_id :name, use: :slugged

end
