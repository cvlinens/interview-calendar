# == Schema Information
#
# Table name: timeframes
#
#  id           :bigint           not null, primary key
#  week_name    :string
#  working_from :string
#  working_to   :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  shift_id     :bigint
#

class Timeframe < ApplicationRecord
  belongs_to :shift
end
