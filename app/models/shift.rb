# == Schema Information
#
# Table name: shifts
#
#  id         :bigint           not null, primary key
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_shifts_on_name  (name) UNIQUE
#  index_shifts_on_slug  (slug) UNIQUE
#

class Shift < ApplicationRecord
  validates_uniqueness_of :name
  has_many :users
  has_many :timeframes
  accepts_nested_attributes_for :timeframes, reject_if: :all_blank, allow_destroy: true

  extend FriendlyId
  friendly_id :name, use: :slugged


  def full_info
    return "#{name} (#{Dates.time_12(working_from, working_to)})"
  end
end
