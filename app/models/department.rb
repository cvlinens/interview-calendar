# == Schema Information
#
# Table name: departments
#
#  id         :bigint           not null, primary key
#  name       :string
#  sort       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :bigint
#

class Department < ApplicationRecord
  has_and_belongs_to_many :users
  scope :managers, -> { where(parent_id: nil).order(name: :asc) }
  scope :all_sort, -> { order(sort: :asc)}
  after_save :update_sort

  def update_sort
    if parent_id.present?
      parent_name = Department.find(parent_id).sort.downcase
      sub_name = self.name.downcase
      str_sort = parent_name + '-' + sub_name
      update_column :sort, str_sort
    else
      update_column :sort, self.name.downcase
    end
  end

  def has_parent?
    self.parent_id.present?
  end

  def has_child?
    Department.where(parent_id: self.id).present?
  end

  def check_level
    temp = self
    index = 1
    until temp.is_root?
      index = index + 1
      temp = Department.find temp.parent_id
    end
    index
  end 

  def is_root?
    self.parent_id.blank?
  end

  def childs
    Department.where(parent_id: self.id)
  end

  def parent
    Department.find_by(id: self.parent_id)
  end

  def related
    Department.where(id: self.id).or(self.childs)      # self and childs
  end

  def related_users
    if self.is_root?
      related_departments = self.related
      array_users = []
      related_departments.each do |d|
        array_users << d.users
      end
      tt = array_users.flatten
    else
      tt = users
    end
    ts = tt.pluck(:id)
    User.where(id: ts)
  end
end
