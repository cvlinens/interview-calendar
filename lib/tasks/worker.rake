namespace :worker do
  desc "TODO"
  task init_point_limits: :environment do
    tz = "Central Time (US & Canada)"
    time_now = Time.now.in_time_zone(tz)
    if time_now.month == 12 && time_now.day == 31
      User.all.each do |user|
        user.user_point_limit = 15
        user.save!
      end
    end
  end

end
