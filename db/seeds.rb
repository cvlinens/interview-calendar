# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Default User: normal
# Department manager: manager
# Admin: admin
['employee', 'manager', 'admin'].each do |role|
  Role.find_or_create_by({name: role})
end

Role.find_each do |role|
  if role.name == 'admin'
    role.full_access = true
    role.user = true
    role.event_creation = true
    role.event_status = true
    role.category = true
    role.shift = true
    role.lunchtime = true
    role.holiday = true
    role.penalty = true
    role.point = true
    role.user_role = true
    role.consequence = true
    role.department = true
  elsif role.name == 'manager'
    role.event_creation = true
    role.event_status = true
  else
    role.event_creation = true
  end
  role.save!
end

eng_department = Department.where(name: "Engineer", sort: "engineer").first_or_create
sales_department = Department.where(name: "Sales", sort: "sales").first_or_create

admin1 = User.where(username: "admin1").first_or_initialize
admin1.email = "devnull1@devnull.com"
admin1.password = 'admin1'
admin1.first_name = "Jeff"
admin1.last_name = "Bezos (admin)"
admin1.role_id = Role.find_by({name: 'admin'}).id
admin1.save!

manager1 = User.where(username: "manager1").first_or_initialize
manager1.email = "devnull2@devnull.com"
manager1.password = 'manager1'
manager1.first_name = "Elon"
manager1.last_name = "Musk (manager1)"
manager1.role_id = Role.find_by({name: 'manager'}).id
manager1.departments << eng_department
manager1.save!

u1 = User.where(username: 'employee1').first_or_initialize
u1.email = "devnull4@devnull.com"
u1.password = 'employee1'
u1.first_name = "Bob"
u1.last_name = "(employee1)"
u1.manager_id = manager1.id
u1.departments << eng_department
u1.role_id = Role.find_by({name: 'employee'}).id
u1.save!


manager2 = User.where(username: "manager2").first_or_initialize
manager2.email = "devnull3@devnull.com"
manager2.password = 'manager2'
manager2.first_name = "Papa"
manager2.last_name = "John (manager2)"
manager2.role_id = Role.find_by({name: 'manager'}).id
manager2.departments << sales_department
manager2.save!


u2 = User.where(username: 'employee2').first_or_initialize
u2.email = "devnull5@devnull.com"
u2.password = 'employee2'
u2.first_name = "Michael"
u2.last_name = "M (employee2)"
u2.departments << sales_department
u2.role_id = Role.find_by({name: 'employee'}).id
u2.save!






#-------- Consequence demo data

consequence1 = Consequence.where(reach_point: "3", description: "Documented Verbal Discussion with employee", flag_color: "#ffff00").first_or_create
consequence1.save!

consequence2 = Consequence.where(reach_point: "6", description: "Write-Up", flag_color: "#ff80ff").first_or_create
consequence2.save!

consequence3 = Consequence.where(reach_point: "9", description: "Write-up & 1-day unpaid suspension", flag_color: "#ff8040").first_or_create
consequence3.save!

consequence4 = Consequence.where(reach_point: "12", description: "Write-up warning notice & 2-day unpaid suspension", flag_color: "#ff0000").first_or_create
consequence4.save!

consequence5 = Consequence.where(reach_point: "15", description: "Termination", flag_color: "#000000").first_or_create
consequence5.save!

consequence6 = Consequence.where(reach_point: "20", description: "Special Case", flag_color: "#0000ff").first_or_create


#--------lunchtime-------------
lunchtime = Lunchtime.where(name: 'Demo', lunchtime_from: '11:40', lunchtime_to: '13:50').first_or_create

#--------- Shift--------
shift = Shift.where(name: 'Demo').first_or_create
shift_id = Shift.find_by(name: 'Demo').id

# timeframe1 = Timeframe.new(week_name: 'Monday', working_from: '08:00', working_to: '17:00', shift_id: shift_id)
# timeframe1.save!
# timeframe2 = Timeframe.new(week_name: 'Tuesday', working_from: '09:00', working_to: '18:00', shift_id: shift_id)
# timeframe2.save!
# timeframe3 = Timeframe.new(week_name: 'Wednesday', working_from: '08:00', working_to: '17:00', shift_id: shift_id)
# timeframe3.save!
# timeframe4 = Timeframe.new(week_name: 'Thursday', working_from: '09:00', working_to: '18:00', shift_id: shift_id)
# timeframe4.save!
# timeframe5 = Timeframe.new(week_name: 'Friday', working_from: '08:00', working_to: '17:00', shift_id: shift_id)
# timeframe5.save!


category = Category.where(name: 'Vacation', description: 'This is a default option', default_option: true, slug: 'vacation').first_or_create
Category.where(name: 'Sick', description: 'This is a default option', default_option: true, slug: 'sick').first_or_create



# ----- POINTS
point_late = Point.where(name: "Late", description: "Arrive Late", penalty_point: 2).first_or_create
point_no_show = Point.where(name: "No Show", description: "Did not show up to work", penalty_point: 8).first_or_create
point_no_clock_out = Point.where(name: "No Clock-out", description: "Did not clock out during breaks", penalty_point: 3).first_or_create

Penalty.create(user_id: u1.id, point_id: point_late.id, description: "..", apply_date: 0.day.ago)
Penalty.create(user_id: u1.id, point_id: point_no_show.id, description: "..", apply_date: 1.day.ago)
Penalty.create(user_id: u1.id, point_id: point_late.id, description: "..", apply_date: 2.days.ago)
Penalty.create(user_id: u1.id, point_id: point_no_clock_out.id, description: "..", apply_date: 4.days.ago)
Penalty.create(user_id: u1.id, point_id: point_late.id, description: "..", apply_date: 6.days.ago)

Penalty.create(user_id: u2.id, point_id: point_late.id, description: "..", apply_date: 2.day.ago)
Penalty.create(user_id: u2.id, point_id: point_no_show.id, description: "..", apply_date: 4.day.ago)
Penalty.create(user_id: u2.id, point_id: point_late.id, description: "..", apply_date: 8.days.ago)
Penalty.create(user_id: u2.id, point_id: point_no_clock_out.id, description: "..", apply_date: 11.days.ago)
Penalty.create(user_id: u2.id, point_id: point_late.id, description: "..", apply_date: 14.days.ago)