# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200623205246) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.boolean "default_option", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["name"], name: "index_categories_on_name", unique: true
    t.index ["slug"], name: "index_categories_on_slug", unique: true
  end

  create_table "consequences", force: :cascade do |t|
    t.integer "reach_point"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "flag_color", default: "#FFFFFF"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.bigint "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sort"
  end

  create_table "departments_users", id: false, force: :cascade do |t|
    t.bigint "department_id", null: false
    t.bigint "user_id", null: false
    t.index ["department_id"], name: "index_departments_users_on_department_id"
    t.index ["user_id"], name: "index_departments_users_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "description"
    t.bigint "user_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer "status", default: 0
    t.bigint "status_by", default: 0
    t.text "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id", default: 1
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "holidays", force: :cascade do |t|
    t.string "name"
    t.datetime "holiday_date"
    t.integer "holidaytype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["slug"], name: "index_holidays_on_slug", unique: true
  end

  create_table "holidaytypes", force: :cascade do |t|
    t.string "name"
    t.integer "hours_off"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "duration"
    t.string "start_time"
    t.string "end_time"
    t.index ["slug"], name: "index_holidaytypes_on_slug", unique: true
  end

  create_table "lunchtimes", force: :cascade do |t|
    t.string "name"
    t.string "lunchtime_from"
    t.string "lunchtime_to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["name"], name: "index_lunchtimes_on_name", unique: true
    t.index ["slug"], name: "index_lunchtimes_on_slug", unique: true
  end

  create_table "penalties", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "point_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "apply_date"
  end

  create_table "points", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.decimal "penalty_point", precision: 5, scale: 3
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.boolean "disabled", default: false
    t.bigint "original_id"
    t.bigint "updated_user"
    t.index ["name"], name: "index_points_on_name", unique: true
    t.index ["slug"], name: "index_points_on_slug", unique: true
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "full_access", default: false
    t.boolean "user", default: false
    t.boolean "user_role", default: false
    t.boolean "event_creation", default: true
    t.boolean "event_status", default: false
    t.boolean "category", default: false
    t.boolean "holiday", default: false
    t.boolean "shift", default: false
    t.boolean "lunchtime", default: false
    t.boolean "point", default: false
    t.boolean "penalty", default: false
    t.boolean "consequence", default: false
    t.boolean "department", default: false
  end

  create_table "shifts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.index ["name"], name: "index_shifts_on_name", unique: true
    t.index ["slug"], name: "index_shifts_on_slug", unique: true
  end

  create_table "timeframes", force: :cascade do |t|
    t.string "week_name"
    t.string "working_from"
    t.string "working_to"
    t.bigint "shift_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "username", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "role_id"
    t.string "first_name"
    t.string "last_name"
    t.bigint "manager_id"
    t.string "slug"
    t.string "time_zone", default: "UTC"
    t.bigint "shift_id", default: 1
    t.bigint "lunchtime_id", default: 1
    t.decimal "user_point_limit", precision: 5, scale: 3
    t.integer "consequence_id", default: 0
    t.boolean "active", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["role_id"], name: "index_users_on_role_id"
    t.index ["slug"], name: "index_users_on_slug", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "users", "roles"
end
