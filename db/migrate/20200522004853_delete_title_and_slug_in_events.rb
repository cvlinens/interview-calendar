class DeleteTitleAndSlugInEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :title
    remove_column :events, :slug
  end
end
