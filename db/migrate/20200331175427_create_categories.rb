class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.index :name, unique: true
      t.text :description
      t.boolean :default_option, default: false
      t.timestamps
    end
  end
end
