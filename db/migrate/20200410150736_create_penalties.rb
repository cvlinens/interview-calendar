class CreatePenalties < ActiveRecord::Migration[5.1]
  def change
    create_table :penalties do |t|
      t.integer :user_id, limit: 8
      t.integer :point_id
      t.text :description
      t.timestamps
    end
  end
end
