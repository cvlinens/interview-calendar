class AddLunchTimeIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :lunchtime_id, :integer, limit: 8, default: 0
  end
end
