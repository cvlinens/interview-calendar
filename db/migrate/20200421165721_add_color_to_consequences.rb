class AddColorToConsequences < ActiveRecord::Migration[5.1]
  def change
    add_column :consequences, :flag_color, :string, default: "#FFFFFF"
  end
end
