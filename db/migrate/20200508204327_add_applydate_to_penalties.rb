class AddApplydateToPenalties < ActiveRecord::Migration[5.1]
  def change
    add_column :penalties, :apply_date, :datetime
  end
end
