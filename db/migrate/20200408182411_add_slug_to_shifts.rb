class AddSlugToShifts < ActiveRecord::Migration[5.1]
  def change
    add_column :shifts, :slug, :string
    add_index :shifts, :slug, unique: true
  end
end
