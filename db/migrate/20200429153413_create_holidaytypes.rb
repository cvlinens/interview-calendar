class CreateHolidaytypes < ActiveRecord::Migration[5.1]
  def change
    create_table :holidaytypes do |t|
      t.string :name
      t.integer :hours_off
      t.timestamps
    end
  end
end
