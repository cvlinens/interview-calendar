class AddUserPointLimitToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :user_point_limit, :decimal,  precision: 5, scale: 3
  end
end
