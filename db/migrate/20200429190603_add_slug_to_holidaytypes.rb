class AddSlugToHolidaytypes < ActiveRecord::Migration[5.1]
  def change
    add_column :holidaytypes, :slug, :string
    add_index :holidaytypes, :slug, unique: true
  end
end
