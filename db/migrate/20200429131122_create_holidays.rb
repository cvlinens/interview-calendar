class CreateHolidays < ActiveRecord::Migration[5.1]
  def change
    create_table :holidays do |t|
      t.string :name
      t.datetime :holiday_date
      t.integer :holidaytype_id
      t.timestamps
    end
  end
end
