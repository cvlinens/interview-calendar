class AddCategoryIdToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :category_id, :integer, default: 1, limit: 8
  end
end
