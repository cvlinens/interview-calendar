class AddSlugToLunchtimes < ActiveRecord::Migration[5.1]
  def change
    add_column :lunchtimes, :slug, :string
    add_index :lunchtimes, :slug, unique: true
  end
end
