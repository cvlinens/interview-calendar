class CreateConsequences < ActiveRecord::Migration[5.1]
  def change
    create_table :consequences do |t|
      t.integer :reach_point
      t.string :description
      t.timestamps
    end
  end
end
