class CreatePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :points do |t|
      t.string :name
      t.index :name, unique: true
      t.text :description
      t.decimal :penalty_point, precision: 5, scale: 3
      t.timestamps
    end
  end
end
