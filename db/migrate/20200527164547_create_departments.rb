class CreateDepartments < ActiveRecord::Migration[5.1]
  def change
    create_table :departments do |t|
      t.string :name
      t.integer :parent_id, limit: 8
      t.timestamps
    end
  end
end
