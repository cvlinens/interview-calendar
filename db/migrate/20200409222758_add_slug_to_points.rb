class AddSlugToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :slug, :string
    add_index :points, :slug, unique: true
  end
end
