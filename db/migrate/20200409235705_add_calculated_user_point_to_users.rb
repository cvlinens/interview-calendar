class AddCalculatedUserPointToUsers < ActiveRecord::Migration[5.1]
  def change
    current_month = Time.now.month
    last_day_of_month = Time.now.end_of_month.day
    current_day = Time.now.day
    total_point = 15

    ratio = total_point / 12.to_f

    user_get_point = (12 - current_month + 1) * ratio

    if current_day >= last_day_of_month / 2.to_f
      user_get_point = user_get_point - ratio / 2.to_f
    end


    User.find_each do |user|
      user.user_point_limit = user_get_point
      user.save!
    end

  end
end
