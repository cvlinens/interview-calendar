class AddOriginalIdToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :original_id, :integer, limit: 8
  end
end
