class AddTimeToHolidaytypes < ActiveRecord::Migration[5.1]
  def change
    add_column :holidaytypes, :duration, :string
    add_column :holidaytypes, :start_time, :string
    add_column :holidaytypes, :end_time, :string 
  end
end
