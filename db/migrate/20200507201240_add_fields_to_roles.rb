class AddFieldsToRoles < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :full_access, :boolean, default: false
    add_column :roles, :user, :boolean, default: false
    add_column :roles, :user_role, :boolean, default: false
    add_column :roles, :event_creation, :boolean, default: true
    add_column :roles, :event_status, :boolean, default: false
    add_column :roles, :category, :boolean, default: false
    add_column :roles, :holiday, :boolean, default: false
    add_column :roles, :shift, :boolean, default: false
    add_column :roles, :lunchtime, :boolean, default: false
    add_column :roles, :point, :boolean, default: false
    add_column :roles, :penalty, :boolean, default: false
    add_column :roles, :consequence, :boolean, default: false
  end
end
