class AddSortToDepartments < ActiveRecord::Migration[5.1]
  def change
    add_column :departments, :sort, :string
  end
end
