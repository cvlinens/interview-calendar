class AddDepartmentsToRoles < ActiveRecord::Migration[5.1]
  def change
    add_column :roles, :department, :boolean, default: false
  end
end
