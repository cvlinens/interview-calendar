class AddConsequenceIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :consequence_id, :integer, default: 0
  end
end
