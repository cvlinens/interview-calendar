class CreateLunchtimes < ActiveRecord::Migration[5.1]
  def change
    create_table :lunchtimes do |t|
      t.string :name
      t.index :name, unique: true
      t.string :lunchtime_from
      t.string :lunchtime_to
      t.timestamps
    end
  end
end
