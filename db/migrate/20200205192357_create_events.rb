class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.string :description
      t.integer :user_id, limit: 8
      t.datetime :start_date
      t.datetime :end_date
      t.integer :status, default: 0
      t.integer :status_by, default: 0, limit: 8
      t.text   :comment
      t.timestamps
    end
  end
end
