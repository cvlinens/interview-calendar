class ChangDefaultShiftLunchtimeInUsers < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:users, :shift_id, 1)
    change_column_default(:users, :lunchtime_id, 1)
  end
end
