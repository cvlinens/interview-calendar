class AddSlugToHolidays < ActiveRecord::Migration[5.1]
  def change
    add_column :holidays, :slug, :string
    add_index :holidays, :slug, unique: true
  end
end
