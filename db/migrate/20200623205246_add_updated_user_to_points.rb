class AddUpdatedUserToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :updated_user, :integer, limit: 8
  end
end
