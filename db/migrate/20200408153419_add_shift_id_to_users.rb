class AddShiftIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :shift_id, :integer, limit: 8, default: 0
  end
end
