class CreateTimeframes < ActiveRecord::Migration[5.1]
  def change
    create_table :timeframes do |t|
      t.string :week_name
      t.string :working_from
      t.string :working_to
      t.integer :shift_id, limit: 8
      t.timestamps
    end
  end
end
