class AddDisabledToPoints < ActiveRecord::Migration[5.1]
  def change
    add_column :points, :disabled, :boolean, default: false
  end
end
