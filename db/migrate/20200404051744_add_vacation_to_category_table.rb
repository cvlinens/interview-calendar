class AddVacationToCategoryTable < ActiveRecord::Migration[5.1]
  def change
    category = Category.new(name: 'Vacation', description: 'This is a default option', default_option: true, slug: 'vacation')
    category.save
  end
end
