# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  active                 :boolean          default(TRUE)
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  sign_in_count          :integer          default(0), not null
#  slug                   :string
#  time_zone              :string           default("UTC")
#  user_point_limit       :decimal(5, 3)
#  username               :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  consequence_id         :integer          default(0)
#  lunchtime_id           :bigint           default(1)
#  manager_id             :bigint
#  role_id                :bigint
#  shift_id               :bigint           default(1)
#
# Indexes
#
#  index_users_on_email     (email) UNIQUE
#  index_users_on_role_id   (role_id)
#  index_users_on_slug      (slug) UNIQUE
#  index_users_on_username  (username) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (role_id => roles.id)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
